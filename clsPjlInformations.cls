VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsPjlInformations"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
' The private collection object
Private m_ColclsPjlInformations As Collection


Private Sub Class_Initialize()

    Set m_ColclsPjlInformations = New Collection

End Sub


' This sub adds a new clsPjlInformation item to the collection.
Sub Add(ClsItem As clsPjlInformation, Optional VarKey As Variant)

    'TODO: Initialize the new clsPjlInformation item's properties here

    m_ColclsPjlInformations.Add ClsItem, VarKey

End Sub


' This sub removes an item from the collection.
Sub Remove(VarIndex As Variant)

    m_ColclsPjlInformations.Remove VarIndex

End Sub


' This function returns a clsPjlInformation item from the collection. It's the default method.
Function Item(VarIndex As Variant) As clsPjlInformation
Attribute Item.VB_UserMemId = 0

    Set Item = m_ColclsPjlInformations.Item(VarIndex)

End Function


' This property returns the number of items in the collection.
Property Get Count() As Long

    Count = m_ColclsPjlInformations.Count

End Property


' This sub removes all items from the collection.
Sub Clear()

    Set m_ColclsPjlInformations = New Collection

End Sub


' This function adds "For Each" enumeration support. Must have a -4 DispID.
Function NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4

    Set NewEnum = m_ColclsPjlInformations.[_NewEnum]

End Function



