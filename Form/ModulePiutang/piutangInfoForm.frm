VERSION 5.00
Object = "{CDE57A40-8B86-11D0-B3C6-00A0C90AEA82}#1.0#0"; "MSDATGRD.OCX"
Begin VB.Form piutangInfo 
   BackColor       =   &H8000000C&
   BorderStyle     =   0  'None
   Caption         =   "Penjualan"
   ClientHeight    =   9315
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   13470
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   9315
   ScaleWidth      =   13470
   ShowInTaskbar   =   0   'False
   Begin VB.PictureBox pcFoot 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   435
      Left            =   60
      ScaleHeight     =   435
      ScaleWidth      =   13305
      TabIndex        =   3
      Top             =   8790
      Width           =   13305
   End
   Begin VB.PictureBox pcTop 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   435
      Left            =   60
      ScaleHeight     =   435
      ScaleWidth      =   13305
      TabIndex        =   2
      Top             =   60
      Width           =   13305
      Begin VB.CommandButton cmdClose 
         Appearance      =   0  'Flat
         BackColor       =   &H8000000D&
         Caption         =   "X"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   15.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   12900
         MaskColor       =   &H0080FF80&
         TabIndex        =   11
         Top             =   30
         Width           =   345
      End
      Begin VB.Label Label8 
         BackStyle       =   0  'Transparent
         Caption         =   "Piutang"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   60
         TabIndex        =   14
         Top             =   90
         Width           =   2535
      End
   End
   Begin VB.PictureBox pcRight 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   8115
      Left            =   9330
      ScaleHeight     =   8115
      ScaleWidth      =   4035
      TabIndex        =   1
      Top             =   570
      Width           =   4035
      Begin VB.CommandButton cmdFind 
         Appearance      =   0  'Flat
         BackColor       =   &H8000000D&
         Caption         =   "FIND"
         Height          =   435
         Left            =   90
         MaskColor       =   &H0080FF80&
         TabIndex        =   17
         Top             =   3990
         Width           =   3885
      End
      Begin VB.TextBox txtTotHutang 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   15.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   450
         Left            =   90
         Locked          =   -1  'True
         TabIndex        =   15
         Text            =   "0.000 Gr"
         Top             =   6990
         Visible         =   0   'False
         Width           =   3855
      End
      Begin VB.CommandButton btnProcess 
         Appearance      =   0  'Flat
         BackColor       =   &H8000000D&
         Caption         =   "PROCESS"
         Height          =   435
         Left            =   90
         MaskColor       =   &H0080FF80&
         TabIndex        =   10
         Top             =   7590
         Width           =   3885
      End
      Begin VB.TextBox txtTotNetto 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   15.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   450
         Left            =   90
         Locked          =   -1  'True
         TabIndex        =   9
         Text            =   "0.000 Gr"
         Top             =   5640
         Width           =   3855
      End
      Begin VB.TextBox txtCustInfo 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2310
         Left            =   120
         MultiLine       =   -1  'True
         TabIndex        =   6
         Top             =   990
         Width           =   3825
      End
      Begin VB.TextBox txtCustId 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   450
         Left            =   120
         TabIndex        =   5
         Top             =   480
         Width           =   3825
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Total Saldo Hutang"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   90
         TabIndex        =   16
         Top             =   6630
         Visible         =   0   'False
         Width           =   1905
      End
      Begin VB.Label Label7 
         BackColor       =   &H000000FF&
         BackStyle       =   0  'Transparent
         Caption         =   "*"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   120
         Left            =   1170
         TabIndex        =   12
         Top             =   60
         Width           =   105
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Total Saldo Piutang"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   120
         TabIndex        =   8
         Top             =   5310
         Width           =   1935
      End
      Begin VB.Label Label2 
         BackStyle       =   0  'Transparent
         Caption         =   "Summary"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   15.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   120
         TabIndex        =   7
         Top             =   4650
         Width           =   2535
      End
      Begin VB.Line Line1 
         BorderStyle     =   4  'Dash-Dot
         X1              =   30
         X2              =   4050
         Y1              =   4560
         Y2              =   4560
      End
      Begin VB.Label Label1 
         BackStyle       =   0  'Transparent
         Caption         =   "Cust Id"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   120
         TabIndex        =   4
         Top             =   60
         Width           =   2535
      End
   End
   Begin VB.PictureBox pcMain 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   8115
      Left            =   60
      ScaleHeight     =   8115
      ScaleWidth      =   9165
      TabIndex        =   0
      Top             =   570
      Width           =   9165
      Begin VB.ComboBox cboLimit 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   660
         TabIndex        =   19
         Top             =   300
         Width           =   915
      End
      Begin MSDataGridLib.DataGrid DGLIST 
         Height          =   7410
         Left            =   30
         TabIndex        =   18
         Top             =   660
         Width           =   9075
         _ExtentX        =   16007
         _ExtentY        =   13070
         _Version        =   393216
         Appearance      =   0
         HeadLines       =   1
         RowHeight       =   19
         FormatLocked    =   -1  'True
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Verdana"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ColumnCount     =   4
         BeginProperty Column00 
            DataField       =   "tanggal"
            Caption         =   "Tanggal"
            BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
               Type            =   1
               Format          =   "dd MMM,yyyy"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   0
            EndProperty
         EndProperty
         BeginProperty Column01 
            DataField       =   "cust_info"
            Caption         =   "Cust"
            BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
               Type            =   0
               Format          =   ""
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   0
            EndProperty
         EndProperty
         BeginProperty Column02 
            DataField       =   "akhirNetto"
            Caption         =   "Saldo (Gr)"
            BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
               Type            =   1
               Format          =   "#,##0.000"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   0
            EndProperty
         EndProperty
         BeginProperty Column03 
            DataField       =   "id"
            Caption         =   "Faktur"
            BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
               Type            =   0
               Format          =   ""
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   0
            EndProperty
         EndProperty
         SplitCount      =   1
         BeginProperty Split0 
            BeginProperty Column00 
               ColumnWidth     =   1500.095
            EndProperty
            BeginProperty Column01 
               ColumnWidth     =   2505.26
            EndProperty
            BeginProperty Column02 
               Alignment       =   1
               ColumnWidth     =   1800
            EndProperty
            BeginProperty Column03 
               ColumnWidth     =   3000.189
            EndProperty
         EndProperty
      End
      Begin VB.Label Label5 
         BackStyle       =   0  'Transparent
         Caption         =   "Limit :"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   60
         TabIndex        =   20
         Top             =   360
         Width           =   675
      End
   End
   Begin VB.TextBox txtFocus 
      Height          =   285
      Left            =   90
      TabIndex        =   13
      Text            =   "Text1"
      Top             =   150
      Width           =   375
   End
End
Attribute VB_Name = "piutangInfo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Dim clData                  As Collection
Dim clInfoCust              As Collection
Dim rsSaldo                 As New ADODB.Recordset

Private cstModul            As clsCst
Private piutangInfo         As clsPiutangInfo

Private inputEditMode       As Boolean
Private kodeItemBefore      As String

Private Sub btnProcess_Click()
    
    If zValidateBeforeInput("SAVE_DATA") = True Then
        paymentGate.Show
        paymentGate.setMappingCustInfo clInfoCust
        
        txtFocus.SetFocus
        btnProcess.Enabled = False
        Me.Enabled = False
    Else
        MsgBox "Lengkapi Inputan Data", vbInformation
        
        btnProcess.Enabled = True
    End If
    
End Sub

Private Sub cmdClose_Click()
    Unload Me
End Sub

Private Sub cmdFind_Click()
    If Len(txtCustId.Text) = 0 Then
        piutangInfo.getAllSaldoOngoing cboLimit.Text
        Set rsSaldo = piutangInfo.getSaldoInfoList
        mappingShowDataSaldo
    Else
        txtCustId_KeyPress 13
    End If
End Sub

Private Sub txtCustId_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        KeyAscii = 0
        
        Set clInfoCust = Nothing
        Set clInfoCust = cstModul.getCustInfo(Trim(txtCustId.Text))
        If clInfoCust.Count > 0 Then
            piutangInfo.getSaldoByIdCust (Trim(txtCustId.Text)), cboLimit.Text
            Set rsSaldo = piutangInfo.getSaldoInfoList
            mappingShowDataSaldo
            
            zMappingCustInfo clInfoCust
        Else
            Set rsSaldo = Nothing
            
            Set rsSaldo = New ADODB.Recordset
            constructRsSaldo
            mappingShowDataSaldo
            zClearCustInfo
            MsgBox "Data Tidak Ditemukan", vbInformation
        End If
    End If
End Sub

Private Sub Form_Load()
    Me.Left = (Screen.Width - Width) / 2
    Me.Top = 0
        
    Set rsSaldo = New ADODB.Recordset
    Set piutangInfo = New clsPiutangInfo
    
    Me.BackColor = RGB(245, 245, 245)
    pcMain.BackColor = RGB(255, 255, 255)
    pcRight.BackColor = RGB(255, 255, 255)
    pcTop.BackColor = RGB(135, 206, 250)
    pcFoot.BackColor = RGB(135, 206, 250)
    
    zClearCustInfo
    zClearSummary
    
    zConstructorObject
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set pjlModul = Nothing
    DashBoard.ShowMenu
End Sub









'====== BATAS LOGIC AND COMPONENT


Private Sub zConstructorObject()
    Set cstModul = New clsCst
    Set clData = New Collection
    Set clInfoCust = New Collection
    Set rsSaldo = New ADODB.Recordset
    
    constructRsSaldo
End Sub

Private Sub zClearSummary()
    
    cboLimit.Clear
    cboLimit.AddItem "25"
    cboLimit.AddItem "50"
    cboLimit.AddItem "100"
    cboLimit.ListIndex = 0
    
    txtTotNetto.Text = "0.000 Gr"
    txtTotHutang.Text = "0.000 Gr"
End Sub

Private Sub zClearCustInfo()
    txtCustId.Text = ""
    txtCustInfo.Text = ""
End Sub

Private Sub zDefaultTextBox(tb As TextBox)
    tb.ForeColor = &H80000008
End Sub

Private Sub zNotAktifTextBox(tb As TextBox, caption As String)
    If Len(tb.Text) = 0 Then
        tb.ForeColor = &H80000011
        tb.Text = caption
    End If
End Sub

Private Sub zmappingDataToInput(clData As Collection)
    
    If clData.Count > 0 Then
        txtKode.Text = Trim(clData.Item(1))
        txtItemName.Text = Trim(clData.Item(2))
        txtTukar.Text = Format(clData.Item(3), "0.00")
        
        If clData.Count > 3 Then
            txtBruto.Text = Format(clData.Item(4), "0.00")
            txtNetto.Text = Format(clData.Item(5), "0.000")
        End If
    Else
        MsgBox "Data Tidak Ditemukan", vbInformation
    End If
End Sub

Private Sub constructRsSaldo()
    rsSaldo.Fields.Append "id", adChar, 35
    rsSaldo.Fields.Append "tanggal", adDate
    rsSaldo.Fields.Append "custInfo", adChar, 35
    rsSaldo.Fields.Append "akhirNetto", adDouble, 18
    rsSaldo.Fields.Append "akhirRp", adDouble, 18
    rsSaldo.Open , , adOpenKeyset, adLockOptimistic
End Sub

Private Sub mappingShowDataSaldo()
    
    Dim iCounter            As Integer
    Dim lv                  As ListItem
    Dim rsTmp               As ADODB.Recordset
    
    txtTotNetto.Text = 0
    If rsSaldo.RecordCount > 0 Then
        '"[strField] DESC, [intField] ASC" #For Multiple Short
        rsSaldo.MoveFirst
        Do
            txtTotNetto.Text = val(Format(txtTotNetto.Text, "0.000")) + val(Format(rsSaldo!akhirNetto, "0.000"))
            rsSaldo.MoveNext
        Loop While rsSaldo.EOF = False
        
        rsSaldo.Sort = "tanggal"
        rsSaldo.MoveFirst
    End If
    Set DGLIST.DataSource = rsSaldo
    DGLIST.Refresh
    
    txtTotNetto.Text = Format(txtTotNetto.Text, "#,##0.000") & " Gr"
End Sub

Private Sub zMappingCustInfo(clData As Collection)
    If clData.Count > 0 Then
        txtCustId.Text = clData.Item(1)
        txtCustInfo.Text = clData.Item(2) & vbCrLf & clData.Item(3) & vbCrLf & clData.Item(4)
    End If
End Sub

Private Function zValidateBeforeInput(typeInput As String) As Boolean
    
    Dim rsData                  As ADODB.Recordset
    
    zValidateBeforeInput = True
    Select Case UCase(typeInput)
           Case "SAVE_DATA"
                If Len(txtCustId.Text) <= 0 Then
                    zValidateBeforeInput = False
                    Exit Function
                End If
    End Select
        
End Function
