VERSION 5.00
Begin VB.Form paymentGate 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Payment- Gate"
   ClientHeight    =   8340
   ClientLeft      =   45
   ClientTop       =   390
   ClientWidth     =   13110
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   8340
   ScaleWidth      =   13110
   Begin VB.PictureBox pcRight 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   7545
      Left            =   8970
      ScaleHeight     =   7545
      ScaleWidth      =   4035
      TabIndex        =   15
      Top             =   90
      Width           =   4035
      Begin VB.TextBox txtFocus 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   480
         Locked          =   -1  'True
         TabIndex        =   26
         Top             =   7560
         Width           =   3255
      End
      Begin VB.TextBox txtTotalBalance 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   15.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   450
         Left            =   60
         Locked          =   -1  'True
         TabIndex        =   24
         Text            =   "0.000 Gr"
         Top             =   5940
         Width           =   3855
      End
      Begin VB.TextBox txtTotalPayment 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   15.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   450
         Left            =   60
         Locked          =   -1  'True
         TabIndex        =   22
         Text            =   "0.000 Gr"
         Top             =   4830
         Width           =   3855
      End
      Begin VB.TextBox txtCustInfo 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2310
         Left            =   120
         Locked          =   -1  'True
         MultiLine       =   -1  'True
         TabIndex        =   18
         Top             =   450
         Width           =   3825
      End
      Begin VB.TextBox txtTotalAmount 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   15.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   450
         Left            =   90
         Locked          =   -1  'True
         TabIndex        =   17
         Text            =   "0.000 Gr"
         Top             =   3960
         Width           =   3855
      End
      Begin VB.CommandButton btnProcess 
         Appearance      =   0  'Flat
         BackColor       =   &H8000000D&
         Caption         =   "PROCESS"
         Height          =   435
         Left            =   90
         MaskColor       =   &H0080FF80&
         TabIndex        =   16
         Top             =   7020
         Width           =   3885
      End
      Begin VB.Label Label10 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Balance (Gr)"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   90
         TabIndex        =   25
         Top             =   5610
         Width           =   3750
      End
      Begin VB.Label Label9 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Total Payment (Gr)"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   90
         TabIndex        =   23
         Top             =   4530
         Width           =   3795
      End
      Begin VB.Label Label8 
         BackStyle       =   0  'Transparent
         Caption         =   "Cust Info"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   120
         TabIndex        =   21
         Top             =   90
         Width           =   2535
      End
      Begin VB.Line Line1 
         BorderStyle     =   4  'Dash-Dot
         X1              =   0
         X2              =   4020
         Y1              =   2880
         Y2              =   2880
      End
      Begin VB.Label Label6 
         BackStyle       =   0  'Transparent
         Caption         =   "Summary"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   15.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   120
         TabIndex        =   20
         Top             =   3000
         Width           =   2535
      End
      Begin VB.Label Label5 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Total Amount (Gr)"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   120
         TabIndex        =   19
         Top             =   3630
         Width           =   3750
      End
   End
   Begin VB.PictureBox mainBoard 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   7545
      Left            =   60
      ScaleHeight     =   7545
      ScaleWidth      =   8835
      TabIndex        =   1
      Top             =   90
      Width           =   8835
      Begin VB.PictureBox picDiscount 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         ForeColor       =   &H80000008&
         Height          =   1155
         Left            =   4890
         ScaleHeight     =   1125
         ScaleWidth      =   3735
         TabIndex        =   27
         TabStop         =   0   'False
         Top             =   2070
         Width           =   3765
         Begin VB.TextBox txtSumNettDiscount 
            Alignment       =   1  'Right Justify
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   270
            Left            =   150
            Locked          =   -1  'True
            TabIndex        =   29
            Text            =   "0.000 Gr"
            Top             =   780
            Width           =   3465
         End
         Begin VB.TextBox txtNettDiscount 
            Alignment       =   1  'Right Justify
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   270
            Left            =   150
            Locked          =   -1  'True
            TabIndex        =   28
            Text            =   "0.000 Gr"
            Top             =   450
            Width           =   3465
         End
         Begin VB.Label lblDiscount 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Discount"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   315
            Left            =   120
            TabIndex        =   30
            Top             =   180
            Width           =   1305
         End
      End
      Begin VB.PictureBox picItem 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         ForeColor       =   &H80000008&
         Height          =   1155
         Left            =   210
         ScaleHeight     =   1125
         ScaleWidth      =   3735
         TabIndex        =   11
         Top             =   2070
         Width           =   3765
         Begin VB.TextBox txtBruto 
            Alignment       =   1  'Right Justify
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   270
            Left            =   150
            Locked          =   -1  'True
            TabIndex        =   13
            Text            =   "0.000 Gr"
            Top             =   450
            Width           =   3465
         End
         Begin VB.TextBox txtNetto 
            Alignment       =   1  'Right Justify
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   270
            Left            =   150
            Locked          =   -1  'True
            TabIndex        =   12
            Text            =   "0.000 Gr"
            Top             =   780
            Width           =   3465
         End
         Begin VB.Label Label3 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Item"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   315
            Left            =   120
            TabIndex        =   14
            Top             =   180
            Width           =   1305
         End
      End
      Begin VB.PictureBox picTransfer 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         ForeColor       =   &H80000008&
         Height          =   1155
         Left            =   4860
         ScaleHeight     =   1125
         ScaleWidth      =   3735
         TabIndex        =   7
         Top             =   600
         Width           =   3765
         Begin VB.TextBox txtTrf 
            Alignment       =   1  'Right Justify
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   270
            Left            =   150
            Locked          =   -1  'True
            TabIndex        =   9
            Text            =   "0.000 Gr"
            Top             =   450
            Width           =   3465
         End
         Begin VB.TextBox txtNettTrf 
            Alignment       =   1  'Right Justify
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   270
            Left            =   150
            Locked          =   -1  'True
            TabIndex        =   8
            Text            =   "0.000 Gr"
            Top             =   780
            Width           =   3465
         End
         Begin VB.Label Label2 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Transfer"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   315
            Left            =   120
            TabIndex        =   10
            Top             =   180
            Width           =   1305
         End
      End
      Begin VB.PictureBox picTunai 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         ForeColor       =   &H80000008&
         Height          =   1155
         Left            =   210
         ScaleHeight     =   1125
         ScaleWidth      =   3735
         TabIndex        =   2
         Top             =   600
         Width           =   3765
         Begin VB.TextBox txtNettTunai 
            Alignment       =   1  'Right Justify
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   270
            Left            =   150
            Locked          =   -1  'True
            TabIndex        =   6
            Text            =   "0.000 Gr"
            Top             =   780
            Width           =   3465
         End
         Begin VB.TextBox txtTunai 
            Alignment       =   1  'Right Justify
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   270
            Left            =   150
            Locked          =   -1  'True
            TabIndex        =   5
            Text            =   "0.000 Gr"
            Top             =   450
            Width           =   3465
         End
         Begin VB.Label Label1 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Tunai"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   315
            Left            =   120
            TabIndex        =   3
            Top             =   180
            Width           =   1305
         End
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Metode Pembayaran"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   240
         TabIndex        =   4
         Top             =   210
         Width           =   2250
      End
   End
   Begin VB.PictureBox pcFoot 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   435
      Left            =   60
      ScaleHeight     =   435
      ScaleWidth      =   12945
      TabIndex        =   0
      Top             =   7740
      Width           =   12945
   End
End
Attribute VB_Name = "paymentGate"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private clInfoCust              As New Collection
Private clCountTrf              As New Collection
Private clCountItem             As New Collection
Private rsTunai                 As ADODB.Recordset
Private rsTrfList               As ADODB.Recordset
Private rsItemList              As ADODB.Recordset
Private rsSummary               As ADODB.Recordset

Private paymentTrx              As clsPaymentTrx



Public Sub setMappingCustInfo(clData As Collection)
    Set clInfoCust = clData
    If clInfoCust.Count > 0 Then
        txtCustInfo.Text = clData.Item(2) & vbCrLf & clData.Item(3) & vbCrLf & clData.Item(4)
        
        rsSummary.AddNew
        rsSummary.Fields("idCust") = Trim(clData.Item(1))
        rsSummary.Fields("totalAmount") = Format(clData.Item(5), "0.000")
        rsSummary.Update
        
        mappingSummary
    End If
End Sub

Public Sub tunaiPaymentSetData(nilaiRp As Double, ciok As Double, nettTunai As Double)
    If rsTunai.EOF = True Then
        rsTunai.AddNew
    End If
    
    rsTunai.Fields("totalTunai") = val(Format(nilaiRp, "0.0"))
    rsTunai.Fields("ciok") = val(Format(ciok, "0.0"))
    rsTunai.Fields("nettTunai") = val(Format(nettTunai, "0.000"))
    rsTunai.Update
    
    mappingCountTunai
    countTotalPayment
End Sub

Public Sub trfPaymentSetData(rsData As ADODB.Recordset)
    Set rsTrfList = Nothing
    Set rsTrfList = rsData
    
    mappingCountTrf
    countTotalPayment
End Sub

Public Sub itemPaymentSetData(rsData As ADODB.Recordset)
    Set rsItemList = Nothing
    Set rsItemList = rsData
    
    mappingCountItem
    countTotalPayment
End Sub


Private Sub btnProcess_Click()
    Dim statusProcess                    As Boolean
        
    txtFocus.SetFocus
    btnProcess.Enabled = False
    
    DoEvents: Sleep (200)
    statusProcess = procesPayment
    
    If statusProcess = True Then
        MsgBox "Data Berhasil Tersimpan", vbOKOnly
    Else
        MsgBox "Data Gagal Tersimpan", vbOKOnly
    End If
    
    btnProcess.Enabled = True
    Unload Me
End Sub

Private Sub Form_Load()
    ConstructObject
End Sub

Private Sub ConstructObject()
    Set clInfoCust = New Collection
    Set clCountTrf = New Collection
    Set clCountItem = New Collection
    Set rsSummary = New ADODB.Recordset
    Set rsTunai = New ADODB.Recordset
    Set rsTrfList = New ADODB.Recordset
    Set rsItemList = New ADODB.Recordset
    
    rsSummaryInitAtribut
    rsTunaiInitAtribut
    rsTrfListInitAtribut
    rsItemListInitAtribut
End Sub
    
Private Sub rsSummaryInitAtribut()
    rsSummary.Fields.Append "idCust", adChar, 15
    rsSummary.Fields.Append "totalAmount", adDouble, 18
    rsSummary.Fields.Append "totalPayment", adDouble, 18
    rsSummary.Fields.Append "cashAmount", adDouble, 18
    rsSummary.Fields.Append "cashnetto", adDouble, 18
    rsSummary.Fields.Append "transferAmount", adDouble, 18
    rsSummary.Fields.Append "transferNetto", adDouble, 18
    rsSummary.Fields.Append "itemAmount", adDouble, 18
    rsSummary.Fields.Append "itemNetto", adDouble, 18
    rsSummary.Fields.Append "balance", adChar, 10
    rsSummary.Open
End Sub

Private Sub rsTunaiInitAtribut()
    rsTunai.Fields.Append "totalTunai", adDouble, 18
    rsTunai.Fields.Append "ciok", adDouble, 18
    rsTunai.Fields.Append "nettTunai", adDouble, 18
    rsTunai.Open
End Sub

Private Sub rsTrfListInitAtribut()
    rsTrfList.Fields.Append "noUrut", adInteger, 3
    rsTrfList.Fields.Append "accountNumber", adChar, 25
    rsTrfList.Fields.Append "paymentAmount", adDouble, 15
    rsTrfList.Fields.Append "ciokKurs", adDouble, 7
    rsTrfList.Fields.Append "revaluationAmount", adDouble, 18
    rsTrfList.Open
End Sub

Private Sub rsItemListInitAtribut()
    rsItemList.Fields.Append "noUrut", adInteger, 3
    rsItemList.Fields.Append "itemId", adChar, 25
    rsItemList.Fields.Append "itemBruto", adDouble, 15
    rsItemList.Fields.Append "itemTukar", adDouble, 7
    rsItemList.Fields.Append "itemNetto", adDouble, 18
    rsItemList.Open
End Sub

Private Sub countTotalPayment()
    
    rsSummary.Fields("totalPayment") = 0
    If val(rsTunai.RecordCount) > 0 Then
        rsSummary.Fields("totalPayment") = val(Format(rsTunai!nettTunai, "0.000"))
    End If
    
    If val(clCountTrf.Count) > 0 Then
        rsSummary.Fields("totalPayment") = val(Format(rsSummary!totalPayment, "0.000")) + val(Format(clCountTrf.Item(2), "0.000"))
    End If
    
    If val(clCountItem.Count) > 0 Then
        rsSummary.Fields("totalPayment") = val(Format(rsSummary!totalPayment, "0.000")) + val(Format(clCountItem.Item(2), "0.000"))
    End If
    
    rsSummary.Update
        
    mappingSummary
End Sub

Private Sub mappingSummary()
    txtTotalAmount.Text = Format(rsSummary!totalAmount, "#,##0.000") & " Gr"
    txtTotalPayment.Text = Format(val(rsSummary!totalPayment), "#,##0.000") & " Gr"
    
    txtTotalBalance.Text = val(Format(rsSummary!totalAmount, "0.000")) - val(Format(rsSummary!totalPayment, "0.000"))
    txtTotalBalance.Text = Format(txtTotalBalance.Text, "#,##0.000") & " Gr"
End Sub

Private Sub mappingCountTunai()
    If rsTunai.EOF = False Then
        txtTunai.Text = "Rp. " & Format(rsTunai!totalTunai, "#,##0.0")
        txtNettTunai.Text = Format(rsTunai!nettTunai, "#,##0.000") & " Gr"
        
        rsSummary.Fields("cashAmount") = Format(rsTunai!totalTunai, "0.0")
        rsSummary.Fields("cashnetto") = Format(rsTunai!nettTunai, "0.000")
    Else
        txtTunai.Text = "Rp. 0"
        txtNettTunai.Text = "0.000 Gr"
        
        rsSummary.Fields("cashAmount") = 0
        rsSummary.Fields("cashnetto") = 0
    End If
    
    rsSummary.Update
End Sub

Private Sub mappingCountTrf()
    
    txtTrf.Text = ""
    txtNettTrf.Text = ""
    Set clCountTrf = Nothing
    Set clCountTrf = New Collection
    
    If rsTrfList.RecordCount > 0 Then
        rsTrfList.MoveFirst
        Do While rsTrfList.EOF = False
            txtTrf.Text = val(Format(txtTrf.Text, "0.0")) + val(Format(rsTrfList!paymentAmount, "0.0"))
            txtNettTrf.Text = val(Format(txtNettTrf.Text, "0.000")) + val(Format(rsTrfList!revaluationAmount, "0.000"))
            
            rsTrfList.MoveNext
        Loop
        rsTrfList.MoveFirst
        
        clCountTrf.Add Format(txtTrf.Text, "0.0")
        clCountTrf.Add Format(txtNettTrf.Text, "0.000")
        
        rsSummary.Fields("transferAmount") = Format(txtTrf.Text, "0.0")
        rsSummary.Fields("transferNetto") = Format(txtNettTrf.Text, "0.000")
        
        txtTrf.Text = "Rp. " & Format(txtTrf.Text, "#,##0")
        txtNettTrf.Text = Format(txtNettTrf, "#,##0.000") & " Gr"
    Else
        txtTrf.Text = "Rp. 0"
        txtNettTrf.Text = "0.000 Gr"
        
        rsSummary.Fields("transferAmount") = 0
        rsSummary.Fields("transferNetto") = 0
    End If
    
    rsSummary.Update
End Sub

Private Sub mappingCountItem()
    
    txtBruto.Text = ""
    txtNetto.Text = ""
    Set clCountItem = Nothing
    Set clCountItem = New Collection
    
    If rsItemList.RecordCount > 0 Then
        rsItemList.MoveFirst
        Do While rsItemList.EOF = False
            txtBruto.Text = val(Format(txtBruto.Text, "0.000")) + val(Format(rsItemList!itemBruto, "0.000"))
            txtNetto.Text = val(Format(txtNetto.Text, "0.000")) + val(Format(rsItemList!itemNetto, "0.000"))
            
            rsItemList.MoveNext
        Loop
        rsItemList.MoveFirst
        
        clCountItem.Add Format(txtBruto.Text, "0.000")
        clCountItem.Add Format(txtNetto.Text, "0.000")
        
        rsSummary.Fields("itemAmount") = Format(txtBruto.Text, "0.000")
        rsSummary.Fields("itemNetto") = Format(txtNetto.Text, "0.000")
        
        txtBruto.Text = Format(txtBruto.Text, "#,##0.00") & " Gr"
        txtNetto.Text = Format(txtNetto, "#,##0.000") & " Gr"
    Else
        txtBruto.Text = "0.000 Gr"
        txtNetto.Text = "0.000 Gr"
        
        rsSummary.Fields("itemAmount") = 0
        rsSummary.Fields("itemNetto") = 0
    End If
    
    rsSummary.Update
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Unload paymentTunaiFrm
    Unload paymentRekeningFrm
    Unload paymentItemFrm
    Unload paymentDiscountFrm
        
    trxPayment.Enabled = True
    trxPayment.btnProcess.Enabled = True
End Sub

Private Sub mainBoard_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    ResetHover
End Sub

Private Sub picDiscount_Click()
    Call SetParent(paymentDiscountFrm.hwnd, paymentGate.hwnd)
    paymentDiscountFrm.Top = mainBoard.Top + 500
    paymentDiscountFrm.Left = (Me.mainBoard.Width / 2) - (paymentDiscountFrm.Width / 2)
    paymentDiscountFrm.Show
    
    paymentDiscountFrm.findDataDisc Trim$(rsSummary!idCust)
End Sub

Private Sub picDiscount_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    SetHover picDiscount
End Sub

Private Sub picItem_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    SetHover picItem
End Sub

Private Sub picTransfer_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    SetHover picTransfer
End Sub

Private Sub picTunai_Click()
    Call SetParent(paymentTunaiFrm.hwnd, paymentGate.hwnd)
    paymentTunaiFrm.Top = mainBoard.Top + 500
    paymentTunaiFrm.Left = (Me.mainBoard.Width / 2) - (paymentTunaiFrm.Width / 2)
    paymentTunaiFrm.getTunaiValue rsTunai
    paymentTunaiFrm.Show
End Sub

Private Sub picTunai_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    SetHover picTunai
End Sub

Private Sub picTransfer_Click()
    Call SetParent(paymentRekeningFrm.hwnd, paymentGate.hwnd)
    paymentRekeningFrm.Top = mainBoard.Top + 500
    paymentRekeningFrm.Left = (Me.mainBoard.Width / 2) - (paymentRekeningFrm.Width / 2)
    paymentRekeningFrm.Show
    
    If rsTrfList.RecordCount > 0 Then
        paymentRekeningFrm.getDataFormPaymentGet rsTrfList
    End If
End Sub
Private Sub picItem_Click()
    Call SetParent(paymentItemFrm.hwnd, paymentGate.hwnd)
    paymentItemFrm.Top = mainBoard.Top + 500
    paymentItemFrm.Left = (Me.mainBoard.Width / 2) - (paymentItemFrm.Width / 2)
    paymentItemFrm.Show
    
    If rsItemList.RecordCount > 0 Then
        paymentItemFrm.getDataFormPaymentGet rsItemList
    End If
End Sub

Private Sub ResetHover()
    mainBoard.BackColor = &H80000005
    picTunai.BackColor = &H80000005
    picTransfer.BackColor = &H80000005
    picItem.BackColor = &H80000005
    picDiscount.BackColor = &H80000005
End Sub

Private Sub SetHover(picMenu As PictureBox)
    picMenu.BackColor = RGB(173, 216, 230)
End Sub

Private Function procesPayment() As Boolean
    Set paymentTrx = New clsPaymentTrx
    
    procesPayment = paymentTrx.savePaymentTrx(rsSummary, rsTunai, rsTrfList, rsItemList)
    
    Set paymentTrx = Nothing
End Function
