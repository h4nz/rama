VERSION 5.00
Begin VB.Form trxKategoriMenu 
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   5850
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   16995
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   5850
   ScaleWidth      =   16995
   ShowInTaskbar   =   0   'False
   Begin VB.PictureBox picKatPembayaran 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   2625
      Left            =   270
      ScaleHeight     =   2625
      ScaleWidth      =   16335
      TabIndex        =   3
      Top             =   3060
      Width           =   16335
      Begin VB.PictureBox picRptPayment 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         ForeColor       =   &H80000008&
         Height          =   2025
         Left            =   2940
         ScaleHeight     =   1995
         ScaleWidth      =   2265
         TabIndex        =   6
         Top             =   300
         Width           =   2295
         Begin VB.Label Label3 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "LIHAT PEMBAYARAN"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   14.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   765
            Left            =   90
            TabIndex        =   7
            Top             =   1200
            Width           =   1995
         End
      End
      Begin VB.PictureBox pembayaranCust 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         ForeColor       =   &H80000008&
         Height          =   2025
         Left            =   270
         ScaleHeight     =   1995
         ScaleWidth      =   2265
         TabIndex        =   4
         Top             =   300
         Width           =   2295
         Begin VB.Label Label2 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "PEMBAYARAN"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   14.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   375
            Left            =   90
            TabIndex        =   5
            Top             =   1530
            Width           =   2055
         End
      End
   End
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   2625
      Left            =   270
      ScaleHeight     =   2625
      ScaleWidth      =   16335
      TabIndex        =   0
      Top             =   180
      Width           =   16335
      Begin VB.PictureBox picPiutangRpt 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         ForeColor       =   &H80000008&
         Height          =   2025
         Left            =   5670
         ScaleHeight     =   1995
         ScaleWidth      =   2265
         TabIndex        =   10
         Top             =   300
         Width           =   2295
         Begin VB.Label Label5 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "LIHAT PIUTANG"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   14.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   705
            Left            =   90
            TabIndex        =   11
            Top             =   1200
            Width           =   2085
         End
      End
      Begin VB.PictureBox picPenjualanRpt 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         ForeColor       =   &H80000008&
         Height          =   2025
         Left            =   2970
         ScaleHeight     =   1995
         ScaleWidth      =   2265
         TabIndex        =   8
         Top             =   300
         Width           =   2295
         Begin VB.Label Label4 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "LIHAT PENJUALAN"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   14.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   765
            Left            =   90
            TabIndex        =   9
            Top             =   1200
            Width           =   1995
         End
      End
      Begin VB.PictureBox picPenjualan 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         ForeColor       =   &H80000008&
         Height          =   2025
         Left            =   270
         ScaleHeight     =   1995
         ScaleWidth      =   2265
         TabIndex        =   1
         Top             =   300
         Width           =   2295
         Begin VB.Label Label1 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "PENJUALAN"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   15.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   375
            Left            =   90
            TabIndex        =   2
            Top             =   1530
            Width           =   2055
         End
      End
   End
End
Attribute VB_Name = "trxKategoriMenu"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False


Private Sub Form_Load()
    nowDate = Format(Now, "YYYY-MM-DD")
End Sub

Private Sub pembayaranCust_Click()
    trxPayment.Show
    Unload Me
End Sub
    
Private Sub picPenjualan_Click()
    trxPjlForm.Show
    Unload Me
End Sub

Private Sub picPenjualanRpt_Click()
    penjualanInfoForm.Show
    Unload Me
End Sub

Private Sub picPenjualanRpt_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    picPenjualanRpt.BackColor = RGB(173, 216, 230)
End Sub

Private Sub picPiutangRpt_Click()
    piutangInfo.Show
    Unload Me
End Sub

Private Sub picPiutangRpt_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    picPiutangRpt.BackColor = RGB(173, 216, 230)
End Sub

Private Sub picRptPayment_Click()
    paymentInfo.Show
    Unload Me
End Sub

Private Sub picRptPayment_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    picRptPayment.BackColor = RGB(173, 216, 230)
End Sub

'MouseEvent
Private Sub Picture1_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    ResetHover
End Sub
Private Sub picKatPembayaran_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    ResetHover
End Sub
Private Sub picPenjualan_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    picPenjualan.BackColor = RGB(173, 216, 230)
End Sub

Private Sub pembayaranCust_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    pembayaranCust.BackColor = RGB(173, 216, 230)
End Sub


Private Sub ResetHover()
    picPenjualan.BackColor = &H80000005
    pembayaranCust.BackColor = &H80000005
    picRptPayment.BackColor = &H80000005
    picPiutangRpt.BackColor = &H80000005
    picPenjualanRpt.BackColor = &H80000005
End Sub
