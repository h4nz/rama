VERSION 5.00
Begin VB.Form loginFrm 
   Appearance      =   0  'Flat
   BackColor       =   &H8000000B&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Login"
   ClientHeight    =   5865
   ClientLeft      =   45
   ClientTop       =   390
   ClientWidth     =   8835
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5865
   ScaleWidth      =   8835
   StartUpPosition =   2  'CenterScreen
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   4005
      Left            =   2250
      ScaleHeight     =   4005
      ScaleWidth      =   4245
      TabIndex        =   0
      Top             =   420
      Width           =   4245
      Begin VB.CommandButton btnLogin 
         Appearance      =   0  'Flat
         BackColor       =   &H8000000D&
         Caption         =   "Login"
         Height          =   435
         Left            =   360
         MaskColor       =   &H0080FF80&
         TabIndex        =   4
         Top             =   3240
         Width           =   3555
      End
      Begin VB.TextBox txtPassword 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   450
         IMEMode         =   3  'DISABLE
         Left            =   390
         PasswordChar    =   "*"
         TabIndex        =   3
         Text            =   "passwrod"
         Top             =   1950
         Width           =   3525
      End
      Begin VB.TextBox txtUsername 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   450
         Left            =   360
         TabIndex        =   1
         Text            =   "username"
         Top             =   1200
         Width           =   3525
      End
      Begin VB.Label Label1 
         BackStyle       =   0  'Transparent
         Caption         =   "Admin Login"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   15.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   1080
         TabIndex        =   2
         Top             =   210
         Width           =   2535
      End
   End
End
Attribute VB_Name = "loginFrm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub btnLogin_Click()
    zValidUser
End Sub

Private Sub Form_Load()
    Me.BackColor = RGB(245, 245, 245)
    Picture1.BackColor = RGB(255, 255, 255)
End Sub

Private Sub txtPassword_GotFocus()
    txtPassword.Text = ""
End Sub

Private Sub txtUsername_GotFocus()
    txtUsername.Text = ""
End Sub












Private Sub zValidUser()
    
    If Len(txtUsername.Text) = 0 Or Len(txtPassword.Text) = 0 Then
        MsgBox "Username dan Password Tidak Boleh Kosong", vbCritical
        Exit Sub
    End If
    
    If txtUsername.Text = "Admin" And txtPassword.Text = "Admin" Then
        DashBoard.Show
        Unload Me
        
        Exit Sub
    End If
    
    MsgBox "Username dan Password Anda Tidak Terdaftar", vbCritical
    Exit Sub
End Sub
