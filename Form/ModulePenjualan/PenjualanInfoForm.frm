VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{CDE57A40-8B86-11D0-B3C6-00A0C90AEA82}#1.0#0"; "MSDATGRD.OCX"
Begin VB.Form penjualanInfoForm 
   BackColor       =   &H8000000C&
   BorderStyle     =   0  'None
   Caption         =   "Lihat Data Penjualan"
   ClientHeight    =   9345
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   14805
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   9345
   ScaleWidth      =   14805
   ShowInTaskbar   =   0   'False
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   435
      Left            =   60
      ScaleHeight     =   435
      ScaleWidth      =   10905
      TabIndex        =   18
      Top             =   8280
      Width           =   10905
      Begin VB.ComboBox cboLimit 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   7560
         TabIndex        =   22
         Top             =   90
         Width           =   915
      End
      Begin VB.CommandButton cmdNextPage 
         Caption         =   ">"
         Height          =   315
         Left            =   10200
         TabIndex        =   21
         Top             =   90
         Width           =   645
      End
      Begin VB.CommandButton cmdPreviousPage 
         Caption         =   "<"
         Height          =   315
         Left            =   9450
         TabIndex        =   20
         Top             =   90
         Width           =   645
      End
      Begin VB.TextBox txtGoPage 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   5580
         TabIndex        =   19
         Top             =   120
         Width           =   825
      End
      Begin VB.Label Label4 
         BackStyle       =   0  'Transparent
         Caption         =   "Show Rows"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   6450
         TabIndex        =   25
         Top             =   210
         Width           =   975
      End
      Begin VB.Label lblPageInfo 
         BackStyle       =   0  'Transparent
         Caption         =   "1 of  999"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   8580
         TabIndex        =   24
         Top             =   180
         Width           =   795
      End
      Begin VB.Label Label7 
         BackStyle       =   0  'Transparent
         Caption         =   "Go To"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   5010
         TabIndex        =   23
         Top             =   180
         Width           =   525
      End
   End
   Begin VB.PictureBox pcFoot 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   435
      Left            =   60
      ScaleHeight     =   435
      ScaleWidth      =   14685
      TabIndex        =   3
      Top             =   8820
      Width           =   14685
   End
   Begin VB.PictureBox pcTop 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   435
      Left            =   60
      ScaleHeight     =   435
      ScaleWidth      =   14685
      TabIndex        =   2
      Top             =   60
      Width           =   14685
      Begin VB.CommandButton cmdClose 
         Appearance      =   0  'Flat
         BackColor       =   &H8000000D&
         Caption         =   "X"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   15.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   14280
         MaskColor       =   &H0080FF80&
         TabIndex        =   7
         Top             =   30
         Width           =   345
      End
      Begin VB.Label Label8 
         BackStyle       =   0  'Transparent
         Caption         =   "Lihat Data Penjualan"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   60
         TabIndex        =   9
         Top             =   90
         Width           =   3165
      End
   End
   Begin VB.PictureBox pcRight 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   8115
      Left            =   11040
      ScaleHeight     =   8115
      ScaleWidth      =   3705
      TabIndex        =   1
      Top             =   570
      Width           =   3705
      Begin VB.CommandButton btnFind 
         Appearance      =   0  'Flat
         BackColor       =   &H8000000D&
         Caption         =   "FIND"
         Height          =   345
         Left            =   90
         MaskColor       =   &H0080FF80&
         TabIndex        =   16
         Top             =   3450
         Width           =   3465
      End
      Begin VB.TextBox txtAccInfo 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   -1  'True
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000001&
         Height          =   510
         Left            =   180
         Locked          =   -1  'True
         MultiLine       =   -1  'True
         TabIndex        =   11
         Top             =   2370
         Width           =   3315
      End
      Begin VB.TextBox txtSumInfo 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   3390
         Left            =   60
         MultiLine       =   -1  'True
         TabIndex        =   10
         Top             =   4590
         Width           =   3615
      End
      Begin VB.TextBox txtCustId 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   120
         TabIndex        =   5
         Top             =   2010
         Width           =   3435
      End
      Begin MSComCtl2.DTPicker DTPicker2 
         Height          =   405
         Left            =   150
         TabIndex        =   14
         Top             =   1020
         Width           =   3435
         _ExtentX        =   6059
         _ExtentY        =   714
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         CustomFormat    =   "dd-MMM-yyyy"
         Format          =   185401347
         CurrentDate     =   41256
      End
      Begin MSComCtl2.DTPicker DTPicker1 
         Height          =   405
         Left            =   150
         TabIndex        =   15
         Top             =   360
         Width           =   3435
         _ExtentX        =   6059
         _ExtentY        =   714
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         CustomFormat    =   "dd-MMM-yyyy"
         Format          =   185401347
         CurrentDate     =   41256
      End
      Begin VB.Label Label5 
         BackStyle       =   0  'Transparent
         Caption         =   "To"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   150
         TabIndex        =   13
         Top             =   780
         Width           =   2535
      End
      Begin VB.Label Label3 
         BackStyle       =   0  'Transparent
         Caption         =   "Date"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   150
         TabIndex        =   12
         Top             =   120
         Width           =   2535
      End
      Begin VB.Label Label2 
         BackStyle       =   0  'Transparent
         Caption         =   "Summary"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   15.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   90
         TabIndex        =   6
         Top             =   4140
         Width           =   2535
      End
      Begin VB.Line Line1 
         BorderStyle     =   4  'Dash-Dot
         X1              =   30
         X2              =   4050
         Y1              =   3900
         Y2              =   3900
      End
      Begin VB.Label Label1 
         BackStyle       =   0  'Transparent
         Caption         =   "Cust Id"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   120
         TabIndex        =   4
         Top             =   1740
         Width           =   2535
      End
   End
   Begin VB.PictureBox pcMain 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   7665
      Left            =   60
      ScaleHeight     =   7665
      ScaleWidth      =   10905
      TabIndex        =   0
      Top             =   600
      Width           =   10905
      Begin MSDataGridLib.DataGrid DGLIST 
         Height          =   7530
         Left            =   30
         TabIndex        =   17
         Top             =   90
         Width           =   10815
         _ExtentX        =   19076
         _ExtentY        =   13282
         _Version        =   393216
         Appearance      =   0
         HeadLines       =   1
         RowHeight       =   19
         FormatLocked    =   -1  'True
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Verdana"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ColumnCount     =   6
         BeginProperty Column00 
            DataField       =   "tanggal"
            Caption         =   "Tanggal"
            BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
               Type            =   1
               Format          =   "dd. MMMM yyyy"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   0
            EndProperty
         EndProperty
         BeginProperty Column01 
            DataField       =   "custInfo"
            Caption         =   "Cust"
            BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
               Type            =   0
               Format          =   ""
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   0
            EndProperty
         EndProperty
         BeginProperty Column02 
            DataField       =   "sumBruto"
            Caption         =   "Total Bruto"
            BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
               Type            =   1
               Format          =   "#,##0.00"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   0
            EndProperty
         EndProperty
         BeginProperty Column03 
            DataField       =   "sumNetto"
            Caption         =   "Total Netto"
            BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
               Type            =   1
               Format          =   "#,##0.000"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   0
            EndProperty
         EndProperty
         BeginProperty Column04 
            DataField       =   "inputBy"
            Caption         =   "Input By"
            BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
               Type            =   1
               Format          =   "#,##0"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   0
            EndProperty
         EndProperty
         BeginProperty Column05 
            DataField       =   "id"
            Caption         =   "id"
            BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
               Type            =   0
               Format          =   ""
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   0
            EndProperty
         EndProperty
         SplitCount      =   1
         BeginProperty Split0 
            BeginProperty Column00 
               ColumnWidth     =   1800
            EndProperty
            BeginProperty Column01 
               ColumnWidth     =   2505.26
            EndProperty
            BeginProperty Column02 
               Alignment       =   1
               ColumnWidth     =   1800
            EndProperty
            BeginProperty Column03 
               Alignment       =   1
               ColumnWidth     =   1604.976
            EndProperty
            BeginProperty Column04 
               Alignment       =   1
               ColumnWidth     =   1904.882
            EndProperty
            BeginProperty Column05 
               ColumnWidth     =   0
            EndProperty
         EndProperty
      End
   End
   Begin VB.TextBox txtFocus 
      Height          =   285
      Left            =   90
      TabIndex        =   8
      Text            =   "Text1"
      Top             =   150
      Width           =   375
   End
End
Attribute VB_Name = "penjualanInfoForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private rsSummary               As ADODB.Recordset
Private rsJualInfoList          As ADODB.Recordset
Private penjualanInfoListShow   As ADODB.Recordset
Dim clData                      As Collection
Dim clInfoCust                  As Collection


Private cstModul        As clsCst
Private pjlInfo         As clsPjlInformation

Private inputEditMode   As Boolean
Private kodeItemBefore  As String

Private intPage         As Integer
Private pageCount       As Integer

Private Sub btnFind_Click()
    getTrxListInfo
End Sub



Private Sub cboLimit_KeyPress(KeyAscii As Integer)
    If KeyAscii <> 13 Then
        KeyAscii = 0
    End If
End Sub

Private Sub cmdClose_Click()
    Unload Me
End Sub

Private Sub cmdNextPage_Click()
    intPage = intPage + 1
    calculasiPageCount
    mappingDataSum intPage, cboLimit.Text
End Sub

Private Sub cmdPreviousPage_Click()
    intPage = intPage - 1
    calculasiPageCount
    mappingDataSum intPage, cboLimit.Text
End Sub

Private Sub cboLimit_Change()
    calculasiPageCount
    mappingDataSum intPage, cboLimit.Text
End Sub

Private Sub cboLimit_Click()
    cboLimit_Change
End Sub

Private Sub txtGoPage_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        mappingDataSum val(txtGoPage.Text), cboLimit.Text
    End If
End Sub

Private Sub DGLIST_DblClick()
    pjlInfo.PrintBuktiTransaksi DGLIST.Columns(5).Text
End Sub

Private Sub txtCustId_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        KeyAscii = 0
        
        Set clInfoCust = Nothing
        Set clInfoCust = cstModul.getCustInfo(Trim(txtCustId.Text))
        If clInfoCust.Count > 0 Then
            zMappingCustInfo clInfoCust
        Else
            zClearCustInfo
            MsgBox "Data Tidak Ditemukan", vbInformation
        End If
    End If
End Sub

Private Sub Form_Load()
    Me.Left = (Screen.Width - Width) / 2
    Me.Top = 0
        
    Set rsSummary = New ADODB.Recordset
    constructrsSummary
    
    Set pjlInfo = New clsPjlInformation
    
    Me.BackColor = RGB(245, 245, 245)
    pcMain.BackColor = RGB(255, 255, 255)
    pcRight.BackColor = RGB(255, 255, 255)
    pcTop.BackColor = RGB(135, 206, 250)
    pcFoot.BackColor = RGB(135, 206, 250)
    
    DTPicker1.value = Format(Now, "yyyy-MM-dd")
    DTPicker2.value = Format(Now, "yyyy-MM-dd")
    
    zClearCustInfo
    zClearSummary
    
    zConstructorObject
    
    cboLimit.Clear
    cboLimit.AddItem "25"
    cboLimit.AddItem "50"
    cboLimit.AddItem "100"
    cboLimit.ListIndex = 2
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set pjlModul = Nothing
    DashBoard.ShowMenu
End Sub









'====== BATAS LOGIC AND COMPONENT


Private Sub zConstructorObject()
    Set cstModul = New clsCst
    Set clData = New Collection
    Set clInfoCust = New Collection
    constructRsJualInfoList
End Sub

Private Sub constructRsJualInfoList()
    Set rsJualInfoList = New ADODB.Recordset
    rsJualInfoList.Fields.Append "id", adChar, 35
    rsJualInfoList.Fields.Append "tanggal", adDate
    rsJualInfoList.Fields.Append "idCust", adChar, 15
    rsJualInfoList.Fields.Append "custInfo", adChar, 35
    rsJualInfoList.Fields.Append "sumBruto", adDouble, 18
    rsJualInfoList.Fields.Append "sumNetto", adDouble, 18
    rsJualInfoList.Fields.Append "inputBy", adChar, 15
    rsJualInfoList.Fields.Append "dateTime", adDBTime
    rsJualInfoList.Open
End Sub

Private Sub resetJualInfoList()
    rsJualInfoList.Close
End Sub

Private Sub destroyJualInfoList()
    Set rsJualInfoList = Nothing
End Sub

Private Sub constructRsJualInfoListShow()
    Set penjualanInfoListShow = New ADODB.Recordset
    penjualanInfoListShow.Fields.Append "id", adChar, 35
    penjualanInfoListShow.Fields.Append "tanggal", adDate
    penjualanInfoListShow.Fields.Append "idCust", adChar, 15
    penjualanInfoListShow.Fields.Append "custInfo", adChar, 35
    penjualanInfoListShow.Fields.Append "sumBruto", adDouble, 18
    penjualanInfoListShow.Fields.Append "sumNetto", adDouble, 18
    penjualanInfoListShow.Fields.Append "inputBy", adChar, 15
    penjualanInfoListShow.Fields.Append "dateTime", adDBTime
    penjualanInfoListShow.Open
End Sub

Private Sub destroyRsJualInfoListShow()
    Set penjualanInfoListShow = Nothing
End Sub

Private Sub constructrsSummary()
    Set rsSummary = New ADODB.Recordset
    rsSummary.Fields.Append "sumBruto", adDouble, 18
    rsSummary.Fields.Append "sumNetto", adDouble, 18
    rsSummary.Open
    
    rsSummary.AddNew
    rsSummary.Fields("sumBruto") = 0
    rsSummary.Fields("sumNetto") = 0
    rsSummary.Update
End Sub

Private Sub resetRsSummary()
    Set rsSummary = Nothing
    constructrsSummary
    zClearSummary
    
    destroyRsJualInfoListShow
    constructRsJualInfoList
    intPage = 1
    calculasiPageCount
    Set DGLIST.DataSource = Nothing
    DGLIST.Refresh
    
End Sub

Private Sub zClearSummary()
    Dim sumBruto As Double
    Dim sumNetto As Double
    
    If rsSummary.RecordCount > 0 Then
        rsSummary.MoveFirst
        Do While rsSummary.EOF = False
            sumBruto = val(Format(sumBruto, "0.000")) + val(Format(rsSummary!sumBruto, "0.000"))
            sumNetto = val(Format(sumNetto, "0.000")) + val(Format(rsSummary!sumNetto, "0.000"))
            
            rsSummary.MoveNext
        Loop
    Else
        sumBruto = 0
        sumNetto = 0
    End If
    
    txtSumInfo.Text = "Total Bruto : " & vbCrLf & "     " & Format(sumBruto, "#,##0.000") & " Gr" & vbCrLf & vbCrLf & _
                      "Total Netto : " & vbCrLf & "     " & Format(sumNetto, "#,##0.00") & " Gr" & vbCrLf & vbCrLf & _
                      ""
    
End Sub

Private Sub zClearCustInfo()
    txtCustId.Text = ""
    txtAccInfo.Text = "-"
    txtSumInfo.Text = ""
End Sub

Private Sub zDefaultTextBox(tb As TextBox)
    tb.ForeColor = &H80000008
End Sub

Private Sub zNotAktifTextBox(tb As TextBox, caption As String)
    If Len(tb.Text) = 0 Then
        tb.ForeColor = &H80000011
        tb.Text = caption
    End If
End Sub

Private Sub zmappingDataToInput(clData As Collection)
    
    If clData.Count > 0 Then
        txtKode.Text = Trim(clData.Item(1))
        txtItemName.Text = Trim(clData.Item(2))
        txtTukar.Text = Format(clData.Item(3), "0.00")
        
        If clData.Count > 3 Then
            txtBruto.Text = Format(clData.Item(4), "0.00")
            txtNetto.Text = Format(clData.Item(5), "0.000")
        End If
    Else
        MsgBox "Data Tidak Ditemukan", vbInformation
    End If
End Sub

Private Sub zMappingCustInfo(clData As Collection)
    If clData.Count > 0 Then
        txtCustId.Text = clData.Item(1)
        txtAccInfo.Text = clData.Item(2) & vbCrLf & clData.Item(3) & vbCrLf & clData.Item(4)
    End If
End Sub

Private Sub getTrxListInfo()
    
    destroyJualInfoList
    constructRsJualInfoList
    Set rsJualInfoList = pjlInfo.findTrxByRangeDate(Format(DTPicker1.value, "YYYY-MM-DD"), Format(DTPicker2.value, "YYYY-MM-DD"), Trim(txtCustId.Text), 0)
    If rsJualInfoList.RecordCount > 0 Then
        Set rsSummary = rsJualInfoList
        rsJualInfoList.Sort = "tanggal,dateTime"
        rsJualInfoList.MoveFirst
        
        intPage = 1
        txtGoPage.Text = ""
        calculasiPageCount
        mappingDataSum 1, 100
    Else
        resetRsSummary
    End If
    Set rsData = Nothing
    
End Sub

Private Sub mappingDataSum(page As Integer, cacheSize As Integer)
    
    Dim Count                       As Integer
    Dim pageNow                     As Integer
    
    If rsJualInfoList.RecordCount > 0 Then
        
        If page > pageCount Then
            pageNow = pageCount
        ElseIf page < 1 Then
            pageNow = 1
        Else
            pageNow = page
        End If
        
        rsJualInfoList.PageSize = cacheSize
        rsJualInfoList.AbsolutePage = pageNow
    End If
    
    destroyRsJualInfoListShow
    constructRsJualInfoListShow
    
    Count = 1
    With rsJualInfoList
        Do While rsJualInfoList.EOF = False And Count <= rsJualInfoList.PageSize
            penjualanInfoListShow.AddNew
            penjualanInfoListShow.Fields("id") = !id
            penjualanInfoListShow.Fields("tanggal") = !tanggal
            penjualanInfoListShow.Fields("idCust") = !idCust
            penjualanInfoListShow.Fields("custInfo") = !custInfo
            penjualanInfoListShow.Fields("sumBruto") = !sumBruto
            penjualanInfoListShow.Fields("sumNetto") = !sumNetto
            penjualanInfoListShow.Fields("inputBy") = !inputBy
            'rsJualInfoList.Fields("DateTime") = !DateTime
            penjualanInfoListShow.Update
            
            Count = Count + 1
            rsJualInfoList.MoveNext
        Loop
    End With
    
    Set DGLIST.DataSource = penjualanInfoListShow
    DGLIST.Refresh
    
    zClearSummary
End Sub

Private Sub calculasiPageCount()
    pageCount = val(rsJualInfoList.RecordCount) / val(cboLimit.Text)
    
    If val(pageCount) <= 0 Then
        pageCount = 1
    End If
    
    lblPageInfo.caption = intPage & " To " & pageCount
End Sub
