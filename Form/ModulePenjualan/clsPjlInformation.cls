VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsPjlInformation"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Private conData                 As connection
Private SetPrinter              As New ntPrinter
Private recordsetLocal          As rsSaveToLocal

Private penjualanInfoList       As ADODB.Recordset
Private pjlInfoDetailList       As ADODB.Recordset
Private pjlInfoHeadList         As ADODB.Recordset


Private Sub Class_Initialize()
    Set conData = New connection
    Set recordsetLocal = New rsSaveToLocal
    
    constructPenjulanInfoList
End Sub

Private Sub Class_Terminate()
    Set conData = Nothing
    Set penjualanInfoList = Nothing
    Set recordsetLocal = Nothing
End Sub

Public Property Get pjlTrxInfoList() As ADODB.Recordset
   Set pjlTrxInfoList = penjualanInfoList
End Property

Public Sub PrintBuktiTransaksi(id As String)
    Dim RSDTA                   As New ADODB.Recordset
    Dim RSHEAD                  As New ADODB.Recordset
    Dim Query1                  As String
    Dim queryHead               As String
    
    queryHead = "SELECT b.nama,b.alamat,a.id,a.tanggal FROM td_penjualan A INNER JOIN md_customer B ON a.kode_customer = b.id WHERE a.id='" & Trim(id) & "'"
    Set RSHEAD = conData.queryExecution(queryHead)
    
    Query1 = "SELECT * FROM td_penjualan_detail WHERE id='" & Trim(id) & "'"
    Set RSDTA = conData.queryExecution(Query1)
    
    SetPrinter.SetPrinterOrientation ntPortrait, vbPRPSA5, "LAPORAN"
    With PenjualanPrintRecipt
        Set .DataSource = RSDTA
            .Sections("Section2").Controls("transactionDate").caption = Format(RSHEAD!tanggal, "DD-MM-YYYY")
            .Sections("Section2").Controls("transactionId").caption = Trim(RSHEAD!id)
            .Sections("Section2").Controls("customerInformation").caption = Trim(RSHEAD!nama) & "-" & Trim(RSHEAD!alamat)
        
        .Show
    End With
    
    Set RSDTA = Nothing
End Sub

Public Function findTrxByRangeDate(dateStart As Date, dateFrom As Date, idCust As String, limit As Integer) As ADODB.Recordset
    getDataPenjualanByRangeDate dateStart, dateFrom, idCust, limit
    
    Set findTrxByRangeDate = pjlTrxInfoList
End Function

Public Function findTrxDetailWithDiscInfo_1(idTrx As String) As ADODB.Recordset
     Set findTrxDetailWithDiscInfo_1 = getPenjualanDetailWithDisc_01(idTrx)
End Function

'============= BATAS PRIVATE =====================================




Private Sub constructPenjulanInfoList()
    Set penjualanInfoList = New ADODB.Recordset
    penjualanInfoList.Fields.Append "id", adChar, 35
    penjualanInfoList.Fields.Append "tanggal", adDate
    penjualanInfoList.Fields.Append "idCust", adChar, 15
    penjualanInfoList.Fields.Append "custInfo", adChar, 35
    penjualanInfoList.Fields.Append "sumBruto", adDouble, 18
    penjualanInfoList.Fields.Append "sumNetto", adDouble, 18
    penjualanInfoList.Fields.Append "inputBy", adChar, 15
    penjualanInfoList.Fields.Append "dateTime", adDBTime
    penjualanInfoList.Open
End Sub

Private Sub getDataPenjualanByRangeDate(dateStart As Date, dateFrom As Date, idCust As String, limit As Integer)
    Dim rsData                  As ADODB.Recordset
    Dim payLoad                 As String
    Dim filterCust              As String
    Dim limitDataQuery          As String
    
    If Len(idCust) > 0 And Trim(idCust) <> "-" Then
        filterCust = " AND a.kode_customer = '" & Trim(idCust) & "'"
    Else
        filterCust = " "
    End If
    
    If val(limit) <= 0 Then
        limitDataQuery = " LIMIT 1000"
    Else
        limitDataQuery = " LIMIT " & limit
    End If
    
    Set penjualanInfoList = Nothing
    constructPenjulanInfoList
    Set recordsetLocal = New rsSaveToLocal
    payLoad = "SELECT tanggal,kode_customer as idCust,concat(b.nama,'-',b.alamat) as custInfo,a.total_bruto as sumBruto,a.total_netto as sumNetto,a.id " & _
              ",a.created_by as inputBy,a.created_date as dateTime  " & _
              "FROM td_penjualan A JOIN md_customer B ON a.kode_customer = b.id " & _
              "WHERE (a.tanggal BETWEEN '" & Format(dateStart, "YYYY-MM-DD") & "' AND '" & Format(dateFrom, "YYYY-MM-DD") & "') " & _
              "" & filterCust & " ORDER BY a.created_date " & limitDataQuery
    Set rsData = conData.queryExecution(payLoad)
    If rsData.EOF = False Then
        recordsetLocal.saveRsToLocal rsData, "DataPenjualanByRangeDate"
    Else
        recordsetLocal.saveRsToLocal penjualanInfoList, "DataPenjualanByRangeDate"
    End If
    penjualanInfoList.Close
    conData.closeConnection

    recordsetLocal.opeRsFromLocal ("DataPenjualanByRangeDate")
    Set penjualanInfoList = recordsetLocal.getRsResult
    Set recordsetLocal = Nothing
End Sub

Private Function getPenjualanDetailWithDisc_01(idFakur As String) As ADODB.Recordset
    'This Method is for get information penjualan detail,
    'That join to md_item for get Dic variable, for calculate nettDisc
    
    Dim rsData                  As ADODB.Recordset
    Dim payLoad                 As String
    
    Set recordsetLocal = New rsSaveToLocal
    payLoad = "SELECT a.id,id_barang as idBarang,bruto as bruto,b.discount as disc FROM td_penjualan_detail A JOIN md_item B ON a.id_barang = b.id " & _
              "WHERE a.id = '" & Trim$(idFakur) & "'"
    Set rsData = conData.queryExecution(payLoad)
    If rsData.EOF = False Then
        recordsetLocal.saveRsToLocal rsData, "PenjualanDetailWithDisc_01"
    Else
        Set rsData = Nothing
        conData.closeConnection
        Set getPenjualanDetailWithDisc_01 = Nothing
    End If
    Set rsData = Nothing
    conData.closeConnection

    recordsetLocal.opeRsFromLocal ("PenjualanDetailWithDisc_01")
    Set getPenjualanDetailWithDisc_01 = recordsetLocal.getRsResult
    Set recordsetLocal = Nothing
    
End Function
