VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsPjl"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Private totalBruto          As Double
Private totalNetto          As Double
Private iCounterTmp         As Integer
Private statusInput         As Boolean

Private conData             As connection
Dim rsTmp                   As ADODB.Recordset

Private tblPenjualan                As String
Private tblPenjualanDetail          As String

Public Enum tipeDataInput
    addNewData = 0
    editData = 1
End Enum

Private Sub Class_Initialize()
    totalBruto = 0
    totalNetto = 0
    iCounterTmp = 0
    
    tblPenjualan = "td_penjualan"
    tblPenjualanDetail = "td_penjualan_detail"
    
    Set conData = New connection
    Set rsTmp = New ADODB.Recordset
    rsTmp.ActiveConnection = Nothing
    rsTmp.CursorLocation = adUseClient
    rsTmp.LockType = adLockBatchOptimistic
    creteTmpFileds
End Sub


Private Sub creteTmpFileds()
    rsTmp.Fields.Append "id", adChar, 10
    rsTmp.Fields.Append "kodeItem", adChar, 15
    rsTmp.Fields.Append "namaItem", adChar, 50
    rsTmp.Fields.Append "kodeTukar", adChar, 10
    rsTmp.Fields.Append "bruto", adDouble, 18
    rsTmp.Fields.Append "netto", adDouble, 18
    rsTmp.Open
End Sub

Public Function addTmpData(clData As Collection, kodeItemBefore As String, tipeInput As tipeDataInput) As ADODB.Recordset
    
    If findDataTmp(clData) = True Then
        updateDataTmp clData, tipeInput
    Else
        iCounterTmp = iCounterTmp + 1
        rsTmp.AddNew
        rsTmp.Fields("id") = val(iCounterTmp)
        rsTmp.Fields("kodeItem") = Trim(clData.Item(1))
        rsTmp.Fields("namaItem") = Trim(clData.Item(2))
        rsTmp.Fields("kodeTukar") = Trim(clData.Item(3))
        rsTmp.Fields("bruto") = Format(clData.Item(4), "0.00")
        rsTmp.Fields("netto") = Format(clData.Item(5), "0.000")
        rsTmp.Update
    End If
    
    Select Case tipeInput
           Case editData
                If clData.Count > 0 Then
                    If Trim(kodeItemBefore) <> clData.Item(1) Then
                        removeDataTmp Trim(kodeItemBefore)
                    End If
                End If
    End Select
    Set addTmpData = rsTmp
End Function

Public Function updateDataTmp(clData As Collection, tipeInput As tipeDataInput)
    Dim kdrTukar                    As Double
    Dim oldBruto                    As Double
    
    rsTmp.MoveFirst
    rsTmp.Find ("kodeItem='" & Trim(clData.Item(1)) & "'")
    
    If rsTmp.EOF = False Then
        oldBruto = Format(rsTmp.Fields("bruto").value, "0.00")
        oldBruto = val(Format(oldBruto, "0.00")) + Format(clData.Item(4), "0.00")
        
        rsTmp.Fields("kodeItem") = Trim(clData.Item(1))
        rsTmp.Fields("namaItem") = Trim(clData.Item(2))
        rsTmp.Fields("kodeTukar") = Trim(clData.Item(3))
        rsTmp.Fields("bruto") = Format(oldBruto, "0.00")
        rsTmp.Fields("netto") = Format(hitungNettoFromBruto(val(Format(oldBruto, "0.00")), val(Format(rsTmp.Fields("kodeTukar"), "0.00"))), "0.000")
        rsTmp.Update
    End If
End Function

Public Function removeDataTmp(kodeItem As String)
    Dim kdrTukar                    As Double
    
    rsTmp.MoveFirst
    rsTmp.Find ("kodeItem='" & Trim(kodeItem) & "'")
    
    If rsTmp.EOF = False Then
        rsTmp.Delete
    End If
End Function

Public Function findDataTmp(clData As Collection) As Boolean
    Dim rsFind                  As New ADODB.Recordset
    
    findDataTmp = False
    If rsTmp.RecordCount > 0 Then
        rsTmp.MoveFirst
        rsTmp.Find ("kodeItem='" & Trim(clData.Item(1)) & "'")
        If rsTmp.EOF = False Then
            findDataTmp = True
        End If
    End If
    
End Function

Public Function getDataTmp() As ADODB.Recordset
    Set getDataTmp = rsTmp
End Function

Public Function getDataTmpByKodeItem(kodeItem As String) As Collection
    Dim clData                  As Collection
    
    Set clData = New Collection
    If rsTmp.RecordCount > 0 Then
        rsTmp.MoveFirst
        rsTmp.Find ("kodeItem='" & Trim(kodeItem) & "'")
        
        clData.Add Trim(rsTmp!kodeItem)
        clData.Add Trim(rsTmp!namaItem)
        clData.Add Format(rsTmp!kodeTukar, "0.00")
        clData.Add Format(rsTmp!bruto, "0.00")
        clData.Add Format(rsTmp!netto, "0.000")
    End If
    
    Set getDataTmpByKodeItem = clData
    Set clData = Nothing
End Function

Public Sub setZeroValue()
    totalBruto = 0
    totalNetto = 0
End Sub

Public Property Let totBruto(value As Double)
    totalBruto = Format(val(Format(totalBruto, "0.000")) + val(Format(value, "0.000")), "0.000")
End Property

Public Property Let totNetto(value As Double)
    totalNetto = Format(val(Format(totalNetto, "0.000")) + val(Format(value, "0.000")), "0.000")
End Property

Public Function getTotalBrutoAndNetto() As Collection
    Dim data        As Collection
    
    Set data = New Collection
    
    data.Add Format(totalBruto, "#,##0.00") & " Gr"
    data.Add Format(totalNetto, "#,##0.000") & " Gr"
    
    Set getTotalBrutoAndNetto = data
End Function

Public Function saveTransaction(custId As String) As Boolean
    Dim idTransaction               As String
    Dim pjlInformation              As clsPjlInformation
    Dim piutang                     As clsPiutangTrx
    Dim clData                      As Collection
    
    Set pjlInformation = New clsPjlInformation
    Set piutang = New clsPiutangTrx
    idTransaction = GetGUID
    
    'Ini Default Untuk Inject CreatedBy and Date
    Set clData = New Collection
    clData.Add Trim(custId)
    clData.Add Format(totalBruto, "0.00")
    clData.Add Format(totalNetto, "0.000")
    clData.Add "Admin"
    
    
    conData.openConnection
    conData.objConn.BeginTrans
    
On Error GoTo saveTransaction_Error
    savePenjualanDetail idTransaction
    savePenjualanHead idTransaction, clData
    
    'Piutang Area
    conData.queryExecutionForTransaction (piutang.writeNewPiutangSaldo("in", Now, Trim(clData.Item(1)), idTransaction, Format(totalNetto, "0.0"), 0))
    conData.queryExecutionForTransaction (piutang.savePiutangLog(Now, debit, idTransaction, Format(totalNetto, "0.000"), 0, idTransaction, "PENJUALAN"))
    
    conData.objConn.CommitTrans
    saveTransaction = True
    
    pjlInformation.PrintBuktiTransaksi idTransaction
    
exit_save_transaction:
    conData.closeConnection
    Set pjlInformation = Nothing
    Set clData = Nothing
    
    
    Exit Function

saveTransaction_Error:
    conData.objConn.RollbackTrans
    saveTransaction = False
    Resume exit_save_transaction
    Exit Function
    
End Function

Private Function savePenjualanDetail(id As String) As Boolean
    
    Dim noUrut                  As Integer
    Dim payLoad                 As String
    Dim payloadDetail           As String
    
    rsTmp.MoveFirst
    Do While rsTmp.EOF = False
        noUrut = noUrut + 1
        payLoad = "('" & Trim(id) & "','" & val(noUrut) & "','" & Trim(rsTmp!kodeItem) & "','" & Format(rsTmp!kodeTukar, "0.00") & "','" & Format(rsTmp!bruto, "0.00") & "','" & Format(rsTmp!netto, "0.000") & "')"
        
        If val(noUrut) = 1 Then
            payloadDetail = payLoad
        Else
            payloadDetail = payloadDetail & "," & payLoad
        End If
        
        rsTmp.MoveNext
    Loop
    
    payLoad = "INSERT INTO " & Trim(tblPenjualanDetail) & " (id,no_urut,id_barang,tukar_jual,bruto,netto) VALUES "
    payloadDetail = payLoad & payloadDetail
    
    conData.queryExecutionForTransaction payloadDetail
End Function

Private Function savePenjualanHead(id As String, clData As Collection) As Boolean
        
    Dim payLoad                 As String
    Dim payLoadPenjualan        As String
    
    payLoad = "('" & Trim(id) & "','" & Format(Now, "YYYY-MM-DD") & "','" & Trim(clData.Item(1)) & "','" & Format(clData.Item(2), "0.00") & "','" & Format(clData.Item(3), "0.000") & "', " & _
              "'" & Trim(clData.Item(4)) & "','" & Format(Now, "YYYY-MM-DD HH:MM:SS") & "')"
    payLoadPenjualan = "INSERT INTO " & Trim(tblPenjualan) & " (id,tanggal,kode_customer,total_bruto,total_netto,created_by,created_date) VALUES " & payLoad
    
    conData.queryExecutionForTransaction payLoadPenjualan
End Function



