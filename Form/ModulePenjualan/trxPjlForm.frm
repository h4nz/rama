VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form trxPjlForm 
   BackColor       =   &H8000000C&
   BorderStyle     =   0  'None
   Caption         =   "Penjualan"
   ClientHeight    =   7560
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   13470
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7560
   ScaleWidth      =   13470
   ShowInTaskbar   =   0   'False
   Begin VB.PictureBox pcFoot 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   435
      Left            =   60
      ScaleHeight     =   435
      ScaleWidth      =   13305
      TabIndex        =   4
      Top             =   7050
      Width           =   13305
   End
   Begin VB.PictureBox pcTop 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   435
      Left            =   60
      ScaleHeight     =   435
      ScaleWidth      =   13305
      TabIndex        =   3
      Top             =   60
      Width           =   13305
      Begin VB.CommandButton cmdClose 
         Appearance      =   0  'Flat
         BackColor       =   &H8000000D&
         Caption         =   "X"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   15.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   12900
         MaskColor       =   &H0080FF80&
         TabIndex        =   20
         Top             =   30
         Width           =   345
      End
   End
   Begin VB.PictureBox pcRight 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   6405
      Left            =   9330
      ScaleHeight     =   6405
      ScaleWidth      =   4035
      TabIndex        =   2
      Top             =   570
      Width           =   4035
      Begin VB.CommandButton btnProcess 
         Appearance      =   0  'Flat
         BackColor       =   &H8000000D&
         Caption         =   "PROCESS"
         Height          =   435
         Left            =   90
         MaskColor       =   &H0080FF80&
         TabIndex        =   13
         Top             =   5880
         Width           =   3885
      End
      Begin VB.TextBox txtTotNetto 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   15.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   450
         Left            =   90
         Locked          =   -1  'True
         TabIndex        =   12
         Text            =   "0.000 Gr"
         Top             =   5130
         Width           =   3855
      End
      Begin VB.TextBox txtTotBruto 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   15.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   450
         Left            =   90
         Locked          =   -1  'True
         TabIndex        =   10
         Text            =   "0.00 Gr"
         Top             =   4380
         Width           =   3855
      End
      Begin VB.TextBox txtCustInfo 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2460
         Left            =   120
         MultiLine       =   -1  'True
         TabIndex        =   7
         Top             =   990
         Width           =   3825
      End
      Begin VB.TextBox txtCustId 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   450
         Left            =   120
         TabIndex        =   6
         Top             =   480
         Width           =   3825
      End
      Begin VB.Label Label7 
         BackColor       =   &H000000FF&
         BackStyle       =   0  'Transparent
         Caption         =   "*"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   120
         Left            =   1170
         TabIndex        =   23
         Top             =   60
         Width           =   105
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Netto"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   180
         TabIndex        =   11
         Top             =   4860
         Width           =   555
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Bruto"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   150
         TabIndex        =   9
         Top             =   4110
         Width           =   525
      End
      Begin VB.Label Label2 
         BackStyle       =   0  'Transparent
         Caption         =   "Summary"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   15.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   120
         TabIndex        =   8
         Top             =   3630
         Width           =   2535
      End
      Begin VB.Line Line1 
         BorderStyle     =   4  'Dash-Dot
         X1              =   30
         X2              =   4050
         Y1              =   3600
         Y2              =   3600
      End
      Begin VB.Label Label1 
         BackStyle       =   0  'Transparent
         Caption         =   "Cust Id"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   120
         TabIndex        =   5
         Top             =   60
         Width           =   2535
      End
   End
   Begin VB.PictureBox pcMain 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   6405
      Left            =   60
      ScaleHeight     =   6405
      ScaleWidth      =   9165
      TabIndex        =   0
      Top             =   570
      Width           =   9165
      Begin VB.TextBox txtNetto 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BeginProperty DataFormat 
            Type            =   0
            Format          =   "#,##0.000"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000011&
         Height          =   360
         Left            =   7800
         TabIndex        =   19
         Text            =   "10,000.000"
         Top             =   600
         Width           =   1305
      End
      Begin VB.TextBox txtBruto 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BeginProperty DataFormat 
            Type            =   0
            Format          =   "#,##0.00"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000011&
         Height          =   360
         Left            =   6540
         TabIndex        =   18
         Text            =   "10,000.00"
         Top             =   600
         Width           =   1185
      End
      Begin VB.TextBox txtTukar 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000011&
         Height          =   360
         Left            =   5580
         TabIndex        =   17
         Text            =   "99.00"
         Top             =   600
         Width           =   885
      End
      Begin VB.TextBox txtItemName 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000011&
         Height          =   360
         Left            =   1800
         TabIndex        =   16
         Text            =   "Item Name"
         Top             =   600
         Width           =   3675
      End
      Begin VB.TextBox txtKode 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000011&
         Height          =   360
         Left            =   90
         TabIndex        =   15
         Text            =   "Kode"
         Top             =   600
         Width           =   1635
      End
      Begin VB.CommandButton cmdAddList 
         Appearance      =   0  'Flat
         BackColor       =   &H8000000D&
         Caption         =   "+"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   15.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   435
         Left            =   8220
         MaskColor       =   &H0080FF80&
         TabIndex        =   14
         Top             =   60
         Width           =   765
      End
      Begin MSComctlLib.ListView lsPenjualan 
         Height          =   5235
         Left            =   90
         TabIndex        =   1
         Top             =   1020
         Width           =   9015
         _ExtentX        =   15901
         _ExtentY        =   9234
         View            =   3
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         FlatScrollBar   =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Verdana"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   6
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "No"
            Object.Width           =   882
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Kode"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "Nama Item"
            Object.Width           =   4586
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Text            =   "Bruto (gr)"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Text            =   "Tukar (%)"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   5
            Text            =   "Netto (gr)"
            Object.Width           =   2540
         EndProperty
      End
      Begin VB.Label Label6 
         BackColor       =   &H000000FF&
         BackStyle       =   0  'Transparent
         Caption         =   "*"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   120
         Left            =   7620
         TabIndex        =   22
         Top             =   450
         Width           =   105
      End
      Begin VB.Label Label5 
         BackColor       =   &H000000FF&
         BackStyle       =   0  'Transparent
         Caption         =   "*"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   120
         Left            =   1590
         TabIndex        =   21
         Top             =   450
         Width           =   105
      End
   End
   Begin VB.TextBox txtFocus 
      Height          =   285
      Left            =   90
      TabIndex        =   24
      Text            =   "Text1"
      Top             =   150
      Width           =   375
   End
End
Attribute VB_Name = "trxPjlForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Dim rsData          As ADODB.Recordset
Dim clData          As Collection

Private pjlModul    As clsPjl
Private cstModul    As clsCst
Private itemModul   As clsItem

Private inputEditMode   As Boolean
Private kodeItemBefore  As String

Private Sub btnProcess_Click()
    txtFocus.SetFocus
    btnProcess.Enabled = False
    
    If zValidateBeforeInput("SAVE_DATA") = True Then
        
        If (pjlModul.saveTransaction(Trim(txtCustId.Text))) = True Then
            zInputConstructor False
            zClearCustInfo
            zClearSummary
            zConstructorObject
            
            MsgBox "Data Berhasil Diproses", vbOKOnly
        Else
            btnProcess.Enabled = True
            MsgBox "Data Gagal Diprocess", vbInformation
        End If
    Else
        MsgBox "Lengkapi Inputan Data", vbInformation
        
        btnProcess.Enabled = True
    End If
    
End Sub

Private Sub cmdAddList_Click()
        
    txtFocus.SetFocus
    cmdAddList.Enabled = False
    If zValidateBeforeInput("INPUT_DATA") = True Then
        addToList
        showListData
        zInputConstructor False
    Else
        MsgBox "Lengkapi Inputan Data", vbInformation
        txtKode.SetFocus
        cmdAddList.Enabled = True
    End If
    
End Sub

Private Sub cmdClose_Click()
    
    Unload Me
    
End Sub

Private Sub lsPenjualan_DblClick()
    Dim rowSelected                 As Integer
    
    If lsPenjualan.ListItems.Count > 0 Then
        rowSelected = lsPenjualan.SelectedItem.Index
        With lsPenjualan.ListItems(rowSelected)
            inputEditMode = True
            zmappingDataToInput pjlModul.getDataTmpByKodeItem(.SubItems(1))
            kodeItemBefore = Trim(.SubItems(1))
        End With
    End If
End Sub

Private Sub lsPenjualan_KeyDown(KeyCode As Integer, Shift As Integer)
    Dim rowSelected                 As Integer
    
    
    If KeyCode = vbKeyDelete Then
        If MsgBox("Yakin Data Transaksi Barang ini Akan Di Hapus ? ", vbQuestion + vbYesNo) = vbNo Then
            Exit Sub
        Else
            
            rowSelected = lsPenjualan.SelectedItem.Index
            With lsPenjualan.ListItems(rowSelected)
                pjlModul.removeDataTmp Trim(.SubItems(1))
                showListData
            End With
            
            Exit Sub
        End If
    End If
End Sub

Private Sub txtCustId_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        KeyAscii = 0
        
        Set clData = cstModul.getCustInfo(Trim(txtCustId.Text))
        If clData.Count > 0 Then
            zMappingCustInfo clData
        Else
            zClearCustInfo
            MsgBox "Data Tidak Ditemukan", vbInformation
        End If
        Set clData = Nothing
    End If
End Sub

Private Sub Form_Load()
    Me.Left = (Screen.Width - Width) / 2
    Me.Top = 0
    
    Me.BackColor = RGB(245, 245, 245)
    pcMain.BackColor = RGB(255, 255, 255)
    pcRight.BackColor = RGB(255, 255, 255)
    pcTop.BackColor = RGB(135, 206, 250)
    pcFoot.BackColor = RGB(135, 206, 250)
    
    zInputConstructor False
    zClearCustInfo
    zClearSummary
    
    zConstructorObject
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set pjlModul = Nothing
    DashBoard.ShowMenu
End Sub

Private Sub txtBruto_Change()
     txtNetto.Text = Format(hitungNettoFromBruto(val(Format(txtBruto.Text, "0.00")), val(Format(txtTukar.Text, "0.00"))), "0.000")
End Sub

Private Sub txtBruto_Click()
    txtBruto_Change
End Sub

Private Sub txtCustId_LostFocus()
    Set clData = cstModul.getCustInfo(Trim(txtCustId.Text))
    If clData.Count > 0 Then
        zMappingCustInfo clData
    Else
        zClearCustInfo
    End If
    Set clData = Nothing
End Sub

Private Sub txtKode_GotFocus()
    txtKode.Text = ""
End Sub

Private Sub txtKode_KeyPress(KeyAscii As Integer)
    zDefaultTextBox txtKode
    
    If KeyAscii = vbKeyEscape Then
        zInputConstructor False
    End If
    
    If KeyAscii = 13 Then
        KeyAscii = 0
        
        Set clData = itemModul.getItemById(txtKode.Text)
        If clData.Count > 0 Then
            zmappingDataToInput clData
            txtBruto.SetFocus
        Else
            zInputConstructor True
            MsgBox "Data Tidak Ditemukan", vbInformation
        End If
    End If
End Sub

Private Sub txtKode_LostFocus()
    Set clData = itemModul.getItemById(txtKode.Text)
    If clData.Count > 0 Then
        zmappingDataToInput clData
    Else
        
        If inputEditMode = True Then
            txtKode.Text = kodeItemBefore
            MsgBox "Kode Item Tidak Di Temukan", vbCritical
        Else
            zInputConstructor False
        End If
        
    End If
End Sub

Private Sub txtBruto_GotFocus()
    If inputEditMode = False Then
        txtBruto.Text = ""
    End If
End Sub

Private Sub txtBruto_KeyPress(KeyAscii As Integer)
    bolehInput "0123456789.", KeyAscii
    
    zDefaultTextBox txtBruto
    If KeyAscii = 13 Then
        KeyAscii = 0
        cmdAddList.SetFocus
    End If
End Sub

Private Sub txtBruto_LostFocus()
    
    If Len(txtBruto.Text) = 0 Then
        zNotAktifTextBox txtBruto, "Bruo (Gr)"
    Else
        txtBruto.Text = Format(txtBruto.Text, "#,##0.00")
    End If
End Sub







'====== BATAS LOGIC AND COMPONENT

Private Sub zInputConstructor(Status As Boolean)
    
    If Status = False Then
        txtKode.Text = ""
        zNotAktifTextBox Me.txtKode, "Kode"
    End If
    
    txtBruto.Text = ""
    txtItemName.Text = ""
    txtTukar.Text = ""
    txtBruto.Text = ""
    txtNetto.Text = ""
    
    zNotAktifTextBox Me.txtItemName, "Item Name"
    zNotAktifTextBox Me.txtTukar, "Tukar (%)"
    zNotAktifTextBox Me.txtBruto, "Bruto (Gr)"
    zNotAktifTextBox Me.txtNetto, "Netto (Gr)"
    
    cmdAddList.Enabled = True
    btnProcess.Enabled = True
    
    inputEditMode = False
    kodeItemBefore = "-"
End Sub

Private Sub zConstructorObject()
    Set pjlModul = New clsPjl
    Set cstModul = New clsCst
    Set itemModul = New clsItem
    Set clData = New Collection
End Sub

Private Sub zClearSummary()
    txtTotBruto.Text = "0.00 Gr"
    txtTotNetto.Text = "0.000 Gr"
    
    lsPenjualan.ListItems.Clear
End Sub

Private Sub zClearCustInfo()
    txtCustId.Text = ""
    txtCustInfo.Text = ""
End Sub

Private Sub zDefaultTextBox(tb As TextBox)
    tb.ForeColor = &H80000008
End Sub

Private Sub zNotAktifTextBox(tb As TextBox, caption As String)
    If Len(tb.Text) = 0 Then
        tb.ForeColor = &H80000011
        tb.Text = caption
    End If
End Sub

Private Sub zmappingDataToInput(clData As Collection)
    
    If clData.Count > 0 Then
        txtKode.Text = Trim(clData.Item(1))
        txtItemName.Text = Trim(clData.Item(2))
        txtTukar.Text = Format(clData.Item(3), "0.00")
        
        If clData.Count > 3 Then
            txtBruto.Text = Format(clData.Item(4), "0.00")
            txtNetto.Text = Format(clData.Item(5), "0.000")
        End If
    Else
        zInputConstructor False
        MsgBox "Data Tidak Ditemukan", vbInformation
    End If
End Sub

Private Sub getCountListPenjualan()
    Dim clData          As Collection
    
    Set clData = pjlModul.getTotalBrutoAndNetto
    
    txtTotBruto.Text = clData.Item(1)
    txtTotNetto.Text = clData.Item(2)
End Sub

Private Sub addToList()
    Dim rsTmp       As ADODB.Recordset
    Dim clData      As New Collection
    
    clData.Add Trim(txtKode.Text)
    clData.Add Trim(txtItemName.Text)
    clData.Add Format(txtTukar.Text, "0.00")
    clData.Add Format(txtBruto.Text, "0.00")
    clData.Add Format(txtNetto.Text, "0.000")
    
    Select Case inputEditMode
           Case False
                pjlModul.addTmpData clData, "-", addNewData
           Case True
                pjlModul.addTmpData clData, Trim(kodeItemBefore), editData
    End Select
    Set clData = Nothing
End Sub

Private Sub showListData()
    
    Dim iCounter            As Integer
    Dim lv                  As ListItem
    Dim rsTmp               As ADODB.Recordset
    
    lsPenjualan.ListItems.Clear
    Set rsTmp = pjlModul.getDataTmp
    pjlModul.setZeroValue
    
    rsTmp.MoveFirst
    Do While Not rsTmp.EOF
        iCounter = iCounter + 1
        
        lsPenjualan.ListItems.Add , , iCounter
        lsPenjualan.ListItems(iCounter).SubItems(1) = Trim(rsTmp!kodeItem)
        lsPenjualan.ListItems(iCounter).SubItems(2) = Trim(rsTmp!namaItem)
        lsPenjualan.ListItems(iCounter).SubItems(3) = Format(rsTmp!bruto, "0.00")
        lsPenjualan.ListItems(iCounter).SubItems(4) = Format(rsTmp!kodeTukar, "0.00")
        lsPenjualan.ListItems(iCounter).SubItems(5) = Format(rsTmp!netto, "0.000")
            
        'Count Total
        pjlModul.totBruto = Format(rsTmp!bruto, "0.00")
        pjlModul.totNetto = Format(rsTmp!netto, "0.000")
         
        rsTmp.MoveNext
    Loop
    
    getCountListPenjualan
End Sub

Private Sub zMappingCustInfo(clData As Collection)
    
    If clData.Count > 0 Then
        txtCustId.Text = clData.Item(1)
        txtCustInfo.Text = clData.Item(2) & vbCrLf & clData.Item(3) & vbCrLf & clData.Item(4)
    End If
    
End Sub

Private Function zValidateBeforeInput(typeInput As String) As Boolean
    
    Dim rsData                  As ADODB.Recordset
    
    zValidateBeforeInput = True
    Select Case UCase(typeInput)
           Case "SAVE_DATA"
                If Len(txtCustId.Text) <= 0 Then
                    zValidateBeforeInput = False
                    Exit Function
                End If
                
                Set rsData = pjlModul.getDataTmp
                If rsData.RecordCount <= 0 Then
                    zValidateBeforeInput = False
                    Exit Function
                End If
            Case "INPUT_DATA"
                If Len(txtNetto.Text) <= 0 Or Format(txtNetto.Text, "0.000") <= 0 Then
                    zValidateBeforeInput = False
                    Exit Function
                End If
    End Select
        
End Function
