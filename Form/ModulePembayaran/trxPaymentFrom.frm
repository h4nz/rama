VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form trxPayment 
   BackColor       =   &H8000000C&
   BorderStyle     =   0  'None
   Caption         =   "Penjualan"
   ClientHeight    =   9315
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   13470
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   9315
   ScaleWidth      =   13470
   ShowInTaskbar   =   0   'False
   Begin VB.PictureBox pcFoot 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   435
      Left            =   60
      ScaleHeight     =   435
      ScaleWidth      =   13305
      TabIndex        =   4
      Top             =   8790
      Width           =   13305
   End
   Begin VB.PictureBox pcTop 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   435
      Left            =   60
      ScaleHeight     =   435
      ScaleWidth      =   13305
      TabIndex        =   3
      Top             =   60
      Width           =   13305
      Begin VB.CommandButton cmdClose 
         Appearance      =   0  'Flat
         BackColor       =   &H8000000D&
         Caption         =   "X"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   15.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   12900
         MaskColor       =   &H0080FF80&
         TabIndex        =   12
         Top             =   30
         Width           =   345
      End
      Begin VB.Label Label8 
         BackStyle       =   0  'Transparent
         Caption         =   "Pembayaran"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   60
         TabIndex        =   15
         Top             =   90
         Width           =   2535
      End
   End
   Begin VB.PictureBox pcRight 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   8115
      Left            =   9330
      ScaleHeight     =   8115
      ScaleWidth      =   4035
      TabIndex        =   2
      Top             =   570
      Width           =   4035
      Begin VB.CommandButton btnProcess 
         Appearance      =   0  'Flat
         BackColor       =   &H8000000D&
         Caption         =   "PROCESS"
         Height          =   435
         Left            =   90
         MaskColor       =   &H0080FF80&
         TabIndex        =   11
         Top             =   7590
         Width           =   3885
      End
      Begin VB.TextBox txtTotNetto 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   15.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   450
         Left            =   90
         Locked          =   -1  'True
         TabIndex        =   10
         Text            =   "0.000 Gr"
         Top             =   4620
         Width           =   3855
      End
      Begin VB.TextBox txtCustInfo 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2310
         Left            =   120
         MultiLine       =   -1  'True
         TabIndex        =   7
         Top             =   990
         Width           =   3825
      End
      Begin VB.TextBox txtCustId 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   450
         Left            =   120
         TabIndex        =   6
         Top             =   480
         Width           =   3825
      End
      Begin VB.Label Label7 
         BackColor       =   &H000000FF&
         BackStyle       =   0  'Transparent
         Caption         =   "*"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   120
         Left            =   1170
         TabIndex        =   13
         Top             =   60
         Width           =   105
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Total Saldo Piutang"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   120
         TabIndex        =   9
         Top             =   4290
         Width           =   1935
      End
      Begin VB.Label Label2 
         BackStyle       =   0  'Transparent
         Caption         =   "Summary"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   15.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   120
         TabIndex        =   8
         Top             =   3630
         Width           =   2535
      End
      Begin VB.Line Line1 
         BorderStyle     =   4  'Dash-Dot
         X1              =   30
         X2              =   4050
         Y1              =   3600
         Y2              =   3600
      End
      Begin VB.Label Label1 
         BackStyle       =   0  'Transparent
         Caption         =   "Cust Id"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   120
         TabIndex        =   5
         Top             =   60
         Width           =   2535
      End
   End
   Begin VB.PictureBox pcMain 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   8115
      Left            =   60
      ScaleHeight     =   8115
      ScaleWidth      =   9165
      TabIndex        =   0
      Top             =   570
      Width           =   9165
      Begin MSComctlLib.ListView lsPenjualan 
         Height          =   7935
         Left            =   90
         TabIndex        =   1
         Top             =   90
         Width           =   9015
         _ExtentX        =   15901
         _ExtentY        =   13996
         View            =   3
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         FlatScrollBar   =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Verdana"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   4
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "No"
            Object.Width           =   882
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Tgl Faktur"
            Object.Width           =   3528
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "No Faktur"
            Object.Width           =   7056
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Text            =   "Netto (gr)"
            Object.Width           =   4410
         EndProperty
      End
   End
   Begin VB.TextBox txtFocus 
      Height          =   285
      Left            =   90
      TabIndex        =   14
      Text            =   "Text1"
      Top             =   150
      Width           =   375
   End
End
Attribute VB_Name = "trxPayment"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Dim rsData              As ADODB.Recordset
Dim clData              As Collection
Dim clInfoCust          As Collection


Private cstModul        As clsCst
Private piutangInfo     As clsPiutangInfo

Private inputEditMode   As Boolean
Private kodeItemBefore  As String

Private Sub btnProcess_Click()
    
    If zValidateBeforeInput("SAVE_DATA") = True Then
        paymentGate.Show
        paymentGate.setMappingCustInfo clInfoCust
        
        txtFocus.SetFocus
        btnProcess.Enabled = False
        
        Me.Enabled = False
    Else
        MsgBox "Lengkapi Inputan Data", vbInformation
        
        btnProcess.Enabled = True
    End If
    
End Sub

Private Sub cmdClose_Click()
    Unload Me
End Sub

Private Sub txtCustId_KeyPress(KeyAscii As Integer)
    Dim rsData          As ADODB.Recordset
    
    If KeyAscii = 13 Then
        KeyAscii = 0
        
        Set clInfoCust = Nothing
        Set clInfoCust = cstModul.getCustInfo(Trim(txtCustId.Text))
        If clInfoCust.Count > 0 Then
            piutangInfo.getSaldoByIdCust (Trim(txtCustId.Text)), 0
            Set rsData = piutangInfo.getSaldoInfoList
            showListData rsData
            zMappingCustInfo clInfoCust
        Else
            zClearCustInfo
            MsgBox "Data Tidak Ditemukan", vbInformation
        End If
    End If
End Sub

Private Sub Form_Load()
    Me.Left = (Screen.Width - Width) / 2
    Me.Top = 0
    
    Set piutangInfo = New clsPiutangInfo
    
    Me.BackColor = RGB(245, 245, 245)
    pcMain.BackColor = RGB(255, 255, 255)
    pcRight.BackColor = RGB(255, 255, 255)
    pcTop.BackColor = RGB(135, 206, 250)
    pcFoot.BackColor = RGB(135, 206, 250)
    
    zClearCustInfo
    zClearSummary
    
    zConstructorObject
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set pjlModul = Nothing
    DashBoard.ShowMenu
End Sub









'====== BATAS LOGIC AND COMPONENT


Private Sub zConstructorObject()
    Set cstModul = New clsCst
    Set clData = New Collection
    Set clInfoCust = New Collection
End Sub

Private Sub zClearSummary()
    txtTotNetto.Text = "0.000 Gr"
    lsPenjualan.ListItems.Clear
End Sub

Private Sub zClearCustInfo()
    txtCustId.Text = ""
    txtCustInfo.Text = ""
    lsPenjualan.ListItems.Clear
End Sub

Private Sub zDefaultTextBox(tb As TextBox)
    tb.ForeColor = &H80000008
End Sub

Private Sub zNotAktifTextBox(tb As TextBox, caption As String)
    If Len(tb.Text) = 0 Then
        tb.ForeColor = &H80000011
        tb.Text = caption
    End If
End Sub

Private Sub zmappingDataToInput(clData As Collection)
    
    If clData.Count > 0 Then
        txtKode.Text = Trim(clData.Item(1))
        txtItemName.Text = Trim(clData.Item(2))
        txtTukar.Text = Format(clData.Item(3), "0.00")
        
        If clData.Count > 3 Then
            txtBruto.Text = Format(clData.Item(4), "0.00")
            txtNetto.Text = Format(clData.Item(5), "0.000")
        End If
    Else
        MsgBox "Data Tidak Ditemukan", vbInformation
    End If
End Sub

Private Sub showListData(rsData As ADODB.Recordset)
    
    Dim iCounter            As Integer
    Dim lv                  As ListItem
    Dim rsTmp               As ADODB.Recordset
    
    lsPenjualan.ListItems.Clear
    Set rsTmp = rsData
    txtTotNetto.Text = "0.000 Gr"
    
    If rsTmp.EOF = False Then
        rsTmp.MoveFirst
        Do While Not rsTmp.EOF
            iCounter = iCounter + 1
            
            lsPenjualan.ListItems.Add , , iCounter
            lsPenjualan.ListItems(iCounter).SubItems(1) = Format(rsTmp!tanggal, "YYYY-MM-DD")
            lsPenjualan.ListItems(iCounter).SubItems(2) = Trim(rsTmp!id)
            lsPenjualan.ListItems(iCounter).SubItems(3) = Format(rsTmp!akhirNetto, "0.000")
                   
            txtTotNetto.Text = val(Format(txtTotNetto.Text, "0.000")) + val(Format(rsTmp!akhirNetto, "0.000"))
            rsTmp.MoveNext
        Loop
    End If
    
    clInfoCust.Add Format(txtTotNetto.Text, "0.000")
    txtTotNetto.Text = Format(txtTotNetto.Text, "0.000") & " Gr"
End Sub

Private Sub zMappingCustInfo(clData As Collection)
    If clData.Count > 0 Then
        txtCustId.Text = clData.Item(1)
        txtCustInfo.Text = clData.Item(2) & vbCrLf & clData.Item(3) & vbCrLf & clData.Item(4)
    End If
    
End Sub

Private Function zValidateBeforeInput(typeInput As String) As Boolean
    
    Dim rsData                  As ADODB.Recordset
    
    zValidateBeforeInput = True
    Select Case UCase(typeInput)
           Case "SAVE_DATA"
                If Len(txtCustId.Text) <= 0 Then
                    zValidateBeforeInput = False
                    Exit Function
                End If
    End Select
        
End Function
