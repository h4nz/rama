VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{CDE57A40-8B86-11D0-B3C6-00A0C90AEA82}#1.0#0"; "MSDATGRD.OCX"
Begin VB.Form paymentInfo 
   BackColor       =   &H8000000C&
   BorderStyle     =   0  'None
   Caption         =   "Lihat Data Penjualan"
   ClientHeight    =   9315
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   14805
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   9315
   ScaleWidth      =   14805
   ShowInTaskbar   =   0   'False
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   435
      Left            =   60
      ScaleHeight     =   435
      ScaleWidth      =   10875
      TabIndex        =   18
      TabStop         =   0   'False
      Top             =   8250
      Width           =   10875
      Begin VB.TextBox txtGoPage 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   5580
         TabIndex        =   22
         Top             =   120
         Width           =   825
      End
      Begin VB.CommandButton cmdPrevious 
         Caption         =   "<"
         Height          =   315
         Left            =   9450
         TabIndex        =   21
         Top             =   90
         Width           =   645
      End
      Begin VB.CommandButton cmdNext 
         Caption         =   ">"
         Height          =   315
         Left            =   10200
         TabIndex        =   20
         Top             =   90
         Width           =   645
      End
      Begin VB.ComboBox cboLimit 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   7560
         Style           =   2  'Dropdown List
         TabIndex        =   19
         Top             =   90
         Width           =   915
      End
      Begin VB.Label lblGoTo 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Go To"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   5010
         TabIndex        =   25
         Top             =   180
         Width           =   525
      End
      Begin VB.Label lblOf 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "1 of  999"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   8580
         TabIndex        =   24
         Top             =   180
         Width           =   795
      End
      Begin VB.Label lblShowRows 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Show Rows"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   6450
         TabIndex        =   23
         Top             =   210
         Width           =   975
      End
   End
   Begin VB.PictureBox pcFoot 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   435
      Left            =   60
      ScaleHeight     =   435
      ScaleWidth      =   14685
      TabIndex        =   3
      Top             =   8790
      Width           =   14685
   End
   Begin VB.PictureBox pcTop 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   435
      Left            =   60
      ScaleHeight     =   435
      ScaleWidth      =   14685
      TabIndex        =   2
      Top             =   60
      Width           =   14685
      Begin VB.CommandButton cmdClose 
         Appearance      =   0  'Flat
         BackColor       =   &H8000000D&
         Caption         =   "X"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   15.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   14280
         MaskColor       =   &H0080FF80&
         TabIndex        =   7
         Top             =   30
         Width           =   345
      End
      Begin VB.Label Label8 
         BackStyle       =   0  'Transparent
         Caption         =   "Lihat Data Pembayaran"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   60
         TabIndex        =   9
         Top             =   90
         Width           =   3165
      End
   End
   Begin VB.PictureBox pcRight 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   8115
      Left            =   11040
      ScaleHeight     =   8115
      ScaleWidth      =   3705
      TabIndex        =   1
      Top             =   570
      Width           =   3705
      Begin VB.CommandButton btnFind 
         Appearance      =   0  'Flat
         BackColor       =   &H8000000D&
         Caption         =   "FIND"
         Height          =   345
         Left            =   90
         MaskColor       =   &H0080FF80&
         TabIndex        =   16
         Top             =   3450
         Width           =   3465
      End
      Begin VB.TextBox txtAccInfo 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   -1  'True
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000001&
         Height          =   510
         Left            =   180
         Locked          =   -1  'True
         MultiLine       =   -1  'True
         TabIndex        =   11
         Text            =   "paymentInfo.frx":0000
         Top             =   2370
         Width           =   3315
      End
      Begin VB.TextBox txtSumInfo 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   3390
         Left            =   60
         MultiLine       =   -1  'True
         TabIndex        =   10
         Text            =   "paymentInfo.frx":000D
         Top             =   4590
         Width           =   3615
      End
      Begin VB.TextBox txtCustId 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   120
         TabIndex        =   5
         Top             =   2010
         Width           =   3435
      End
      Begin MSComCtl2.DTPicker DTPicker2 
         Height          =   405
         Left            =   150
         TabIndex        =   14
         Top             =   1020
         Width           =   3435
         _ExtentX        =   6059
         _ExtentY        =   714
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         CustomFormat    =   "dd-MMM-yyyy"
         Format          =   185335811
         CurrentDate     =   41256
      End
      Begin MSComCtl2.DTPicker DTPicker1 
         Height          =   405
         Left            =   150
         TabIndex        =   15
         Top             =   360
         Width           =   3435
         _ExtentX        =   6059
         _ExtentY        =   714
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         CustomFormat    =   "dd-MMM-yyyy"
         Format          =   185335811
         CurrentDate     =   41256
      End
      Begin VB.Label Label5 
         BackStyle       =   0  'Transparent
         Caption         =   "To"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   150
         TabIndex        =   13
         Top             =   780
         Width           =   2535
      End
      Begin VB.Label Label3 
         BackStyle       =   0  'Transparent
         Caption         =   "Date"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   150
         TabIndex        =   12
         Top             =   120
         Width           =   2535
      End
      Begin VB.Label Label2 
         BackStyle       =   0  'Transparent
         Caption         =   "Summary"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   15.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   90
         TabIndex        =   6
         Top             =   4140
         Width           =   2535
      End
      Begin VB.Line Line1 
         BorderStyle     =   4  'Dash-Dot
         X1              =   30
         X2              =   4050
         Y1              =   3900
         Y2              =   3900
      End
      Begin VB.Label Label1 
         BackStyle       =   0  'Transparent
         Caption         =   "Cust Id"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   120
         TabIndex        =   4
         Top             =   1740
         Width           =   2535
      End
   End
   Begin VB.PictureBox pcMain 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   7665
      Left            =   60
      ScaleHeight     =   7665
      ScaleWidth      =   10875
      TabIndex        =   0
      Top             =   570
      Width           =   10875
      Begin MSDataGridLib.DataGrid DGLIST 
         Height          =   7560
         Left            =   30
         TabIndex        =   17
         Top             =   60
         Width           =   10755
         _ExtentX        =   18971
         _ExtentY        =   13335
         _Version        =   393216
         Appearance      =   0
         HeadLines       =   1
         RowHeight       =   19
         FormatLocked    =   -1  'True
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Verdana"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ColumnCount     =   7
         BeginProperty Column00 
            DataField       =   "tanggal"
            Caption         =   "Tanggal"
            BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
               Type            =   1
               Format          =   "d. MMM yyyy"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   0
            EndProperty
         EndProperty
         BeginProperty Column01 
            DataField       =   "cust_info"
            Caption         =   "Cust"
            BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
               Type            =   0
               Format          =   ""
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   0
            EndProperty
         EndProperty
         BeginProperty Column02 
            DataField       =   "sum_payment_netto"
            Caption         =   "Total Payment (Gr)"
            BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
               Type            =   1
               Format          =   "#,##0.000"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   0
            EndProperty
         EndProperty
         BeginProperty Column03 
            DataField       =   "sum_cash_amount"
            Caption         =   "Cash Amount (Rp)"
            BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
               Type            =   1
               Format          =   "#,##0"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   0
            EndProperty
         EndProperty
         BeginProperty Column04 
            DataField       =   "sum_trf_amount"
            Caption         =   "Transfer Amount (Rp)"
            BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
               Type            =   1
               Format          =   "#,##0"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   0
            EndProperty
         EndProperty
         BeginProperty Column05 
            DataField       =   "sum_item_bruto"
            Caption         =   "Item Amount (Gr)"
            BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
               Type            =   1
               Format          =   "#,##0.00"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   0
            EndProperty
         EndProperty
         BeginProperty Column06 
            DataField       =   "id"
            Caption         =   "id"
            BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
               Type            =   0
               Format          =   ""
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   0
            EndProperty
         EndProperty
         SplitCount      =   1
         BeginProperty Split0 
            BeginProperty Column00 
               ColumnWidth     =   1500.095
            EndProperty
            BeginProperty Column01 
               ColumnWidth     =   1800
            EndProperty
            BeginProperty Column02 
               Alignment       =   1
               ColumnWidth     =   1800
            EndProperty
            BeginProperty Column03 
               Alignment       =   1
               ColumnWidth     =   1604.976
            EndProperty
            BeginProperty Column04 
               Alignment       =   1
               ColumnWidth     =   1904.882
            EndProperty
            BeginProperty Column05 
               Alignment       =   1
            EndProperty
            BeginProperty Column06 
               ColumnWidth     =   0
            EndProperty
         EndProperty
      End
   End
   Begin VB.TextBox txtFocus 
      Height          =   285
      Left            =   90
      TabIndex        =   8
      Text            =   "Text1"
      Top             =   150
      Width           =   375
   End
End
Attribute VB_Name = "paymentInfo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private rsSummary       As ADODB.Recordset
Dim clData              As Collection
Dim clInfoCust          As Collection


Private cstModul        As clsCst
Private paymentInfo     As clspaymentTrxInfo

Private inputEditMode   As Boolean
Private kodeItemBefore  As String

Private Sub btnFind_Click()
    getSumPaymentInfo
End Sub

Private Sub cboLimit_KeyPress(KeyAscii As Integer)
    If KeyAscii <> 13 Then
        KeyAscii = 0
    End If
End Sub

Private Sub cmdClose_Click()
    Unload Me
End Sub

Private Sub DGLIST_DblClick()
    paymentInfo.paymentTrxPrintRecipt DGLIST.Columns(6).Text
End Sub

Private Sub txtCustId_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        KeyAscii = 0
        
        Set clInfoCust = Nothing
        Set clInfoCust = cstModul.getCustInfo(Trim(txtCustId.Text))
        If clInfoCust.Count > 0 Then
            zMappingCustInfo clInfoCust
        Else
            zClearCustInfo
            MsgBox "Data Tidak Ditemukan", vbInformation
        End If
    End If
End Sub

Private Sub Form_Load()
    Me.Left = (Screen.Width - Width) / 2
    Me.Top = 0
        
    Set rsSummary = New ADODB.Recordset
    constructrsSummary
    
    Set paymentInfo = New clspaymentTrxInfo
    
    Me.BackColor = RGB(245, 245, 245)
    pcMain.BackColor = RGB(255, 255, 255)
    pcRight.BackColor = RGB(255, 255, 255)
    pcTop.BackColor = RGB(135, 206, 250)
    pcFoot.BackColor = RGB(135, 206, 250)
    
    DTPicker1.value = Format(Now, "yyyy-MM-dd")
    DTPicker2.value = Format(Now, "yyyy-MM-dd")
    
    zClearCustInfo
    zClearSummary
    
    zConstructorObject
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set pjlModul = Nothing
    DashBoard.ShowMenu
End Sub









'====== BATAS LOGIC AND COMPONENT


Private Sub zConstructorObject()
    Set cstModul = New clsCst
    Set clData = New Collection
    Set clInfoCust = New Collection
End Sub

Private Sub constructrsSummary()
    rsSummary.Fields.Append "sum_payment_netto", adDouble, 18
    rsSummary.Fields.Append "sum_item_bruto", adDouble, 18
    rsSummary.Fields.Append "sum_cash_amount", adDouble, 18
    rsSummary.Fields.Append "sum_trf_amount", adDouble, 18
    rsSummary.Open
    
    rsSummary.AddNew
    rsSummary.Fields("sum_payment_netto") = 0
    rsSummary.Fields("sum_item_bruto") = 0
    rsSummary.Fields("sum_cash_amount") = 0
    rsSummary.Fields("sum_trf_amount") = 0
    rsSummary.Update
End Sub

Private Sub resetRsSummary()
    rsSummary.Close
    constructrsSummary
End Sub

Private Sub zClearSummary()
    Dim sumPay24K As Double
    Dim sumPay18K As Double
    Dim sumPayCsh As Double
    Dim sumPayTrf As Double
    
    If rsSummary.RecordCount > 0 Then
        rsSummary.MoveFirst
        Do While rsSummary.EOF = False
            sumPay24K = val(Format(sumPay24K, "0.000")) + val(Format(rsSummary!sum_payment_netto, "0.000"))
            sumPay18K = val(Format(sumPay18K, "0.000")) + val(Format(rsSummary!sum_item_bruto, "0.000"))
            sumPayCsh = val(Format(sumPayCsh, "0.0")) + val(Format(rsSummary!sum_cash_amount, "0.0"))
            sumPayTrf = val(Format(sumPayTrf, "0.0")) + val(Format(rsSummary!sum_trf_amount, "0.0"))
            
            rsSummary.MoveNext
        Loop
    Else
        sumPay24K = 0
        sumPay18K = 0
        sumPayCsh = 0
        sumPayTrf = 0
    End If
    
    txtSumInfo.Text = "Total Payment (24K) : " & vbCrLf & "     " & Format(sumPay24K, "#,##0.000") & " Gr" & vbCrLf & vbCrLf & _
                      "Total Payment Bruto : " & vbCrLf & "     " & Format(sumPay18K, "#,##0.00") & " Gr" & vbCrLf & vbCrLf & _
                      "Total Payment Cash  : " & vbCrLf & " Rp. " & Format(sumPayCsh, "#,##0") & vbCrLf & vbCrLf & _
                      "Total Payment Trf   : " & vbCrLf & " Rp. " & Format(sumPayTrf, "#,##0") & vbCrLf & vbCrLf & _
                      ""
    
'    cboLimit.AddItem "25"
'    cboLimit.AddItem "50"
'    cboLimit.AddItem "100"
'    cboLimit.Text = 50
End Sub

Private Sub zClearCustInfo()
    txtCustId.Text = ""
    txtAccInfo.Text = "-"
    txtSumInfo.Text = ""
End Sub

Private Sub zDefaultTextBox(tb As TextBox)
    tb.ForeColor = &H80000008
End Sub

Private Sub zNotAktifTextBox(tb As TextBox, caption As String)
    If Len(tb.Text) = 0 Then
        tb.ForeColor = &H80000011
        tb.Text = caption
    End If
End Sub

Private Sub zmappingDataToInput(clData As Collection)
    
    If clData.Count > 0 Then
        txtKode.Text = Trim(clData.Item(1))
        txtItemName.Text = Trim(clData.Item(2))
        txtTukar.Text = Format(clData.Item(3), "0.00")
        
        If clData.Count > 3 Then
            txtBruto.Text = Format(clData.Item(4), "0.00")
            txtNetto.Text = Format(clData.Item(5), "0.000")
        End If
    Else
        MsgBox "Data Tidak Ditemukan", vbInformation
    End If
End Sub

Private Sub zMappingCustInfo(clData As Collection)
    If clData.Count > 0 Then
        txtCustId.Text = clData.Item(1)
        txtAccInfo.Text = clData.Item(2) & vbCrLf & clData.Item(3) & vbCrLf & clData.Item(4)
    End If
End Sub

Private Sub getSumPaymentInfo()
    Dim rsData                  As New ADODB.Recordset
    Dim rsRequest               As New ADODB.Recordset
    
    Set rsRequest = paymentInfo.getPayloadDto
    rsRequest.Fields("dateFrom") = Format(DTPicker1.value, "YYYY-MM-DD")
    rsRequest.Fields("dateTo") = Format(DTPicker2.value, "YYYY-MM-DD")
    rsRequest.Fields("idCust") = Trim(txtCustId.Text)
    rsRequest.Update
    paymentInfo.setPayloadRequestValue rsRequest
    
    Set rsData = paymentInfo.processGetDataPaymentSum
    paymentInfo.setNothinResultTmp
    On Error GoTo resetSummary

    If rsData.RecordCount > 0 Then
        Set rsSummary = rsData
    Else
goToResetSummary:
        resetRsSummary
    End If
    
    mappingDataSum rsData
    Set rsData = Nothing
    
    Exit Sub
    
resetSummary:
    Resume goToResetSummary
    
End Sub

Private Sub mappingDataSum(rsData As ADODB.Recordset)
    Set DGLIST.DataSource = rsData
    DGLIST.Refresh
    
    zClearSummary
End Sub
