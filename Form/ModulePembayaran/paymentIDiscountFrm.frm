VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form paymentDiscountFrm 
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   7680
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   8250
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7680
   ScaleWidth      =   8250
   ShowInTaskbar   =   0   'False
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   7545
      Left            =   60
      ScaleHeight     =   7545
      ScaleWidth      =   8115
      TabIndex        =   0
      Top             =   60
      Width           =   8115
      Begin VB.CommandButton cmdExit 
         Appearance      =   0  'Flat
         Caption         =   "Exit"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   120
         TabIndex        =   5
         Top             =   6870
         Width           =   1335
      End
      Begin VB.CommandButton cmdAddTunai 
         Appearance      =   0  'Flat
         Caption         =   "Add"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   6690
         TabIndex        =   4
         Top             =   6870
         Width           =   1335
      End
      Begin VB.PictureBox Picture2 
         Appearance      =   0  'Flat
         BackColor       =   &H80000002&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   645
         Left            =   0
         ScaleHeight     =   645
         ScaleWidth      =   8925
         TabIndex        =   1
         Top             =   0
         Width           =   8925
         Begin VB.Label Label1 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Discount"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   15.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   465
            Left            =   150
            TabIndex        =   2
            Top             =   120
            Width           =   4335
         End
      End
      Begin MSComctlLib.ListView lsData 
         Height          =   6045
         Left            =   60
         TabIndex        =   3
         Top             =   720
         Width           =   7965
         _ExtentX        =   14049
         _ExtentY        =   10663
         View            =   3
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         FlatScrollBar   =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Verdana"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   9
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "No"
            Object.Width           =   882
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "No Faktur"
            Object.Width           =   6174
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   2
            Text            =   "Disc (gr)"
            Object.Width           =   4410
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   5
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   6
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   7
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   8
            Object.Width           =   2540
         EndProperty
      End
   End
End
Attribute VB_Name = "paymentDiscountFrm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private rsDiscDetailList                    As ADODB.Recordset

Dim noUrutList                              As Integer

Private discInfo                            As discountInfo


Private Sub cmdAddTunai_Click()
    'paymentGate.itemPaymentSetData rsItemList
    Unload Me
End Sub

Private Sub lsData_DblClick()
    Dim rowSelected                 As Integer
    
    If lsData.ListItems.Count > 0 Then
        rowSelected = lsData.SelectedItem.Index
        With lsData.ListItems(rowSelected)
            showList False, Trim(.SubItems(1))
        End With
    End If
End Sub




Private Sub Form_Load()
    cmdAddTunai.TabIndex = 5: cmdExit.TabIndex = 6
    Set rsDiscDetailList = New ADODB.Recordset
    Set discInfo = New discountInfo
    
'    rsRekAccInitAtribut
'    rsTrfListInitAtribut
'    zInputConstructor
End Sub

Private Sub cmdExit_Click()
    Unload Me
End Sub


Public Sub findDataDisc(idCust As String)
    discInfo.getDiscAllByIdCust Trim$(idCust)
    Set rsDiscDetailList = discInfo.discListDetail
    showList True, "-"
End Sub

Public Sub getDataFormPaymentGet(rsData As ADODB.Recordset)
'    Set rsItemList = rsData
'    showList
End Sub




'====== End Of Commponet Code Line

Private Sub rsTrfListInitAtribut()
    rsDiscDetailList.Fields.Append "noUrut", adInteger, 3
    rsDiscDetailList.Fields.Append "dateTrx", adDate
    rsDiscDetailList.Fields.Append "noFaktur", adChar, 35
    rsDiscDetailList.Fields.Append "idFaktur", adChar, 35
    rsDiscDetailList.Fields.Append "idItem", adChar, 15
    rsDiscDetailList.Fields.Append "bruto", adDouble, 10
    rsDiscDetailList.Fields.Append "discPersen", adDouble, 10
    rsDiscDetailList.Fields.Append "discNetto", adDouble, 15
    rsDiscDetailList.Open
End Sub

Private Sub zInputConstructor()
    DoEvents
End Sub


Private Sub getAccountInfo()
    If rsDumyItemPayment.RecordCount > 0 Then
        rsDumyItemPayment.MoveFirst
        rsDumyItemPayment.Find ("id='" & Trim(cboAccountNo.Text) & "'")
        If rsDumyItemPayment.EOF = False Then
            txtAccInfo.Text = Trim(rsDumyItemPayment!itemName)
            txtCiokKurs.Text = Format(rsDumyItemPayment!nilaiTukar, "0.00")
        Else
            txtAccInfo.Text = "-"
            txtCiokKurs.Text = 0
        End If
    Else
        txtAccInfo.Text = "-"
        txtCiokKurs.Text = 0
    End If
End Sub

Private Function getNoUrutFromrsTrfList() As Integer
    If rsItemList.RecordCount <= 0 Then
        getNoUrutFromrsTrfList = 1
    Else
        rsItemList.MoveLast
        getNoUrutFromrsTrfList = val(rsItemList!noUrut) + 1
    End If
End Function
    
Private Function validationInput() As Boolean
    validationInput = True
    
    If val(Format(txtPayRevaluation.Text, "0.000")) <= 0 Then
        validationInput = False
    End If
    
    If (txtAccInfo.Text) = "-" Then
        validationInput = False
    End If
End Function
    
Private Sub addToList()
    Dim noUrut                  As Integer
    
    noUrut = val(getNoUrutFromrsTrfList)
    
    rsItemList.AddNew
    rsItemList.Fields("noUrut") = val(noUrut)
    rsItemList.Fields("itemId") = Trim(cboAccountNo.Text)
    rsItemList.Fields("itemBruto") = Format(txtPayAmount.Text, "0.00")
    rsItemList.Fields("itemTukar") = Format(txtCiokKurs.Text, "0.00")
    rsItemList.Fields("itemNetto") = Format(txtPayRevaluation.Text, "0.000")
    rsItemList.Update
End Sub

Private Sub editList(noUrut As Integer)
    
    rsItemList.MoveFirst
    rsItemList.Find ("noUrut='" & Trim(noUrut) & "'")
    
    If rsItemList.EOF = False Then
        rsItemList.Fields("noUrut") = val(noUrut)
        rsItemList.Fields("itemId") = Trim(cboAccountNo.Text)
        rsItemList.Fields("itemBruto") = Format(txtPayAmount.Text, "0.00")
        rsItemList.Fields("itemTukar") = Format(txtCiokKurs.Text, "0.00")
        rsItemList.Fields("itemNetto") = Format(txtPayRevaluation.Text, "0.000")
        rsItemList.Update
    End If
End Sub

Private Sub setListViewHeader(isSum As Boolean)
    With lsData
        .ListItems.Clear
        
        Select Case isSum
              Case True
                    .ColumnHeaders(1).Width = 500
                    .ColumnHeaders(1).Alignment = lvwColumnLeft
                    .ColumnHeaders(1).Text = "No"
                    
                    .ColumnHeaders(2).Width = 3500
                    .ColumnHeaders(2).Alignment = lvwColumnLeft
                    .ColumnHeaders(2).Text = "No Faktur"
                    
                    .ColumnHeaders(3).Width = 2500
                    .ColumnHeaders(3).Alignment = lvwColumnRight
                    .ColumnHeaders(3).Text = "Netto Disc"
                    
                    HideColumntLv Me.lsData, 3
              Case Else
                    .ColumnHeaders(1).Width = 500
                    .ColumnHeaders(1).Alignment = lvwColumnLeft
                    .ColumnHeaders(1).Text = "No"
                    
                    .ColumnHeaders(2).Width = 0
                    .ColumnHeaders(2).Alignment = lvwColumnRight
                    .ColumnHeaders(2).Text = "No Faktur"
                    
                    .ColumnHeaders(3).Width = 1000
                    .ColumnHeaders(3).Alignment = lvwColumnRight
                    .ColumnHeaders(3).Text = "Item"
                    
                    .ColumnHeaders(4).Width = 1500
                    .ColumnHeaders(4).Alignment = lvwColumnRight
                    .ColumnHeaders(4).Text = "Bruto (Gr)"
                    
                    .ColumnHeaders(5).Width = 1000
                    .ColumnHeaders(5).Alignment = lvwColumnRight
                    .ColumnHeaders(5).Text = "Disc(%)"
                    
                    .ColumnHeaders(6).Width = 1000
                    .ColumnHeaders(6).Alignment = lvwColumnRight
                    .ColumnHeaders(6).Text = "Disc (Gr)"
                    
                    .ColumnHeaders(7).Width = 1500
                    .ColumnHeaders(7).Alignment = lvwColumnLeft
                    .ColumnHeaders(7).Text = "Tanggal"
                    
                    .ColumnHeaders(8).Width = 2500
                    .ColumnHeaders(8).Alignment = lvwColumnLeft
                    .ColumnHeaders(8).Text = "No Faktur"
                    
                    HideColumntLv Me.lsData, 8
        End Select
    End With
End Sub

Private Sub showList(isSum As Boolean, idFaktur As String)
    Dim iCounter                        As Integer
    Dim tmpNoFaktur                     As String
    Dim tmpSumNettAmount                As Double
    
    
    setListViewHeader isSum
    If rsDiscDetailList.EOF = False Then
        If isSum = True Then
            With rsDiscDetailList
                .MoveFirst
                .Sort = "idFaktur"
                tmpNoFaktur = Trim(!idFaktur)
                
                lsData.ListItems.Clear
                Do While Not .EOF
                    If Trim$(tmpNoFaktur) = Trim(!idFaktur) Then
                        tmpSumNettAmount = val(Format(tmpSumNettAmount, "0.000")) + val(Format(!discNetto, "0.000"))
                    Else
                        iCounter = iCounter + 1
                        lsData.ListItems.Add , , val(iCounter)
                        
                        .MovePrevious
                        lsData.ListItems(iCounter).SubItems(1) = Trim(!idFaktur)
                        lsData.ListItems(iCounter).SubItems(2) = Format(tmpSumNettAmount, "#,##0.000")
                        lsData.ListItems(iCounter).SubItems(6) = Format(!dateTrx, "YYYY-MM-DD")
                        lsData.ListItems(iCounter).SubItems(7) = Trim(!noFaktur)
                        .MoveNext
                        
                        tmpNoFaktur = Trim(!idFaktur)
                        tmpSumNettAmount = val(Format(!discNetto, "0.000"))
                    End If
                    .MoveNext
                Loop
            End With
        Else
            With rsDiscDetailList
                .MoveFirst
                .Find ("idFaktur='" & Trim(idFaktur) & "'")
                tmpNoFaktur = Trim(!idFaktur)
                
                lsData.ListItems.Clear
                Do While Not .EOF
                    
                    If Trim$(!idFaktur) = Trim(idFaktur) Then
                        iCounter = iCounter + 1
                        lsData.ListItems.Add , , val(iCounter)
                        
                        lsData.ListItems(iCounter).SubItems(1) = Trim(!idFaktur)
                        lsData.ListItems(iCounter).SubItems(2) = Trim(!idItem)
                        lsData.ListItems(iCounter).SubItems(3) = Format(!bruto, "#,##0.00")
                        lsData.ListItems(iCounter).SubItems(4) = Format(!discPersen, "#,##0.00")
                        lsData.ListItems(iCounter).SubItems(5) = Format(!discNetto, "#,##0.000")
                        lsData.ListItems(iCounter).SubItems(6) = Format(!dateTrx, "YYYY-MM-DD")
                        lsData.ListItems(iCounter).SubItems(7) = Trim(!noFaktur)
                    End If
                    
                    .MoveNext
                Loop
            End With
        End If
        rsDiscDetailList.MoveFirst
    End If
End Sub

Private Sub mappingFromListForEdit(noUrut As Integer)
    rsItemList.MoveFirst
    rsItemList.Find ("noUrut='" & Trim(noUrut) & "'")
    
    If rsItemList.EOF = False Then
        cboAccountNo.Text = Trim(rsItemList.Fields("itemId"))
        txtPayAmount.Text = Format(rsItemList.Fields("itemBruto"), "#,##0.00")
        txtCiokKurs.Text = Format(rsItemList.Fields("itemTukar"), "0.00")
        
        cmdAddData.caption = "!"
    End If
End Sub

Private Sub lsData_KeyDown(KeyCode As Integer, Shift As Integer)
    DoEvents
    
    If KeyCode = vbKeyEscape Then
        showList True, "-"
    End If
End Sub
