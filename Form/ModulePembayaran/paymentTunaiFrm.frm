VERSION 5.00
Begin VB.Form paymentTunaiFrm 
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   6030
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   5220
   LinkMode        =   1  'Source
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   6030
   ScaleWidth      =   5220
   ShowInTaskbar   =   0   'False
   Begin VB.PictureBox Picture2 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   5835
      Left            =   90
      ScaleHeight     =   5835
      ScaleWidth      =   5025
      TabIndex        =   0
      Top             =   90
      Width           =   5025
      Begin VB.CommandButton cmdAddTunai 
         Appearance      =   0  'Flat
         Caption         =   "Add"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   3000
         TabIndex        =   10
         Top             =   4950
         Width           =   1335
      End
      Begin VB.TextBox txtNettTunai 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   15.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000001&
         Height          =   495
         Left            =   510
         TabIndex        =   8
         Text            =   "1,000,000,000"
         Top             =   4020
         Width           =   3855
      End
      Begin VB.TextBox txtHargaCiok 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   15.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000001&
         Height          =   495
         Left            =   510
         TabIndex        =   6
         Text            =   "1,000,000,000"
         Top             =   2730
         Width           =   3855
      End
      Begin VB.PictureBox Picture1 
         Appearance      =   0  'Flat
         BackColor       =   &H80000002&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   705
         Left            =   0
         ScaleHeight     =   705
         ScaleWidth      =   6255
         TabIndex        =   4
         Top             =   0
         Width           =   6255
         Begin VB.Label Label1 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Tunai"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   15.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   315
            Left            =   150
            TabIndex        =   5
            Top             =   150
            Width           =   1305
         End
      End
      Begin VB.CommandButton cmdExit 
         Appearance      =   0  'Flat
         Caption         =   "Exit"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   510
         TabIndex        =   3
         Top             =   4950
         Width           =   1335
      End
      Begin VB.TextBox txtTunai 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   15.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000001&
         Height          =   450
         Left            =   510
         TabIndex        =   1
         Text            =   "1,000,000,000"
         Top             =   1650
         Width           =   3855
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Revaluasi Amount (Gr)"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   510
         TabIndex        =   9
         Top             =   3720
         Width           =   2205
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Ciok (Rp)"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   510
         TabIndex        =   7
         Top             =   2430
         Width           =   900
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Payment Amount (Rp)"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   540
         TabIndex        =   2
         Top             =   1350
         Width           =   2190
      End
   End
End
Attribute VB_Name = "paymentTunaiFrm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private rsTunai             As ADODB.Recordset

Public Sub getTunaiValue(rsData As ADODB.Recordset)
    If rsData.EOF = False Then
        Set rsTunai = rsData
    End If
    
    mappingInfoFromPaymentGate
End Sub

Private Sub cmdAddTunai_Click()
    If validationInput = True Then
        paymentGate.tunaiPaymentSetData Format(txtTunai.Text, "0.0"), Format(txtHargaCiok.Text, "0.0"), Format(txtNettTunai, "0.000")
        Unload Me
    Else
        MsgBox "Lengkapi Inputan Data", vbInformation
    End If
End Sub

Private Sub txtHargaCiok_Change()
    txtHargaCiok.Text = Format(txtHargaCiok.Text, "#,##0")
    txtHargaCiok.SelStart = Len(txtHargaCiok.Text) + 1
    tunaiCalculation True
End Sub

Private Sub txtHargaCiok_Click()
    txtTunai_Change
End Sub

Private Sub txtHargaCiok_GotFocus()
    FocusObjek txtHargaCiok
End Sub

Private Sub txtHargaCiok_KeyPress(KeyAscii As Integer)
    bolehInput "7894561230.", KeyAscii
End Sub

Private Sub txtNettTunai_KeyPress(KeyAscii As Integer)
    bolehInput "7894561230.", KeyAscii
End Sub

Private Sub txtTunai_Change()
    txtTunai.Text = Format(txtTunai.Text, "#,##0")
    txtTunai.SelStart = Len(txtTunai.Text) + 1
    tunaiCalculation True
End Sub

Private Sub txtTunai_Click()
    txtTunai_Change
End Sub

Private Sub txtTunai_GotFocus()
    FocusObjek txtTunai
End Sub

Private Sub cmdExit_Click()
    Unload Me
End Sub

Private Sub Form_Load()
    
    txtTunai.TabIndex = 0: txtHargaCiok.TabIndex = 1: cmdAddTunai.TabIndex = 2
    cmdExit.TabIndex = 3
    
    Set rsTunai = New ADODB.Recordset
    
    rsTunaiInitAtribut
End Sub

Private Sub rsTunaiInitAtribut()
    rsTunai.Fields.Append "totalTunai", adDouble, 18
    rsTunai.Fields.Append "ciok", adDouble, 18
    rsTunai.Fields.Append "nettTunai", adDouble, 18
    rsTunai.Open
End Sub

Private Sub mappingInfoFromPaymentGate()
    If rsTunai.EOF = False Then
        txtTunai.Text = Format(Val(rsTunai!totalTunai), "#,##0.0")
        txtHargaCiok.Text = Format(Val(rsTunai!ciok), "#,##0.0")
        txtNettTunai.Text = Format(Val(rsTunai!nettTunai), "#,##0.000")
    Else
        txtTunai.Text = "0.0"
        txtHargaCiok.Text = "0.0"
        txtNettTunai.Text = "0.000"
    End If
End Sub

Private Sub tunaiCalculation(fromTunai As Boolean)
    If (Val(Format(txtTunai.Text, "0.0")) > 0) And (Val(Format(txtHargaCiok.Text, ".00")) > 0) Then
        txtNettTunai.Text = Val(Format(txtTunai.Text, "0.0")) / Val(Format(txtHargaCiok.Text, "0.0"))
    Else
        txtNettTunai.Text = 0
    End If
    txtNettTunai.Text = Val(Format(txtNettTunai.Text, "#,##0.000"))
End Sub

Private Sub txtTunai_KeyPress(KeyAscii As Integer)
    bolehInput "7894561230.", KeyAscii
End Sub

Private Function validationInput() As Boolean
    validationInput = True
    
    If Val(Format(txtNettTunai.Text, "0.000")) <= 0 Then
        validationInput = False
    End If
End Function
