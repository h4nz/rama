VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form paymentItemFrm 
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   7680
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   8250
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7680
   ScaleWidth      =   8250
   ShowInTaskbar   =   0   'False
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   7545
      Left            =   60
      ScaleHeight     =   7545
      ScaleWidth      =   8115
      TabIndex        =   0
      Top             =   60
      Width           =   8115
      Begin VB.ComboBox cboAccountNo 
         Appearance      =   0  'Flat
         Height          =   315
         Left            =   90
         TabIndex        =   15
         Text            =   "Combo1"
         Top             =   1350
         Width           =   2805
      End
      Begin VB.TextBox txtAccInfo 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   -1  'True
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000001&
         Height          =   390
         Left            =   90
         Locked          =   -1  'True
         MultiLine       =   -1  'True
         TabIndex        =   14
         Text            =   "paymentItemFrm.frx":0000
         Top             =   1620
         Width           =   2805
      End
      Begin VB.CommandButton cmdExit 
         Appearance      =   0  'Flat
         Caption         =   "Exit"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   120
         TabIndex        =   9
         Top             =   6870
         Width           =   1335
      End
      Begin VB.CommandButton cmdAddTunai 
         Appearance      =   0  'Flat
         Caption         =   "Add"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   6690
         TabIndex        =   8
         Top             =   6870
         Width           =   1335
      End
      Begin VB.CommandButton cmdAddData 
         Appearance      =   0  'Flat
         Caption         =   "+"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   7650
         TabIndex        =   7
         Top             =   1350
         Width           =   405
      End
      Begin VB.PictureBox Picture2 
         Appearance      =   0  'Flat
         BackColor       =   &H80000002&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   645
         Left            =   0
         ScaleHeight     =   645
         ScaleWidth      =   8925
         TabIndex        =   4
         Top             =   0
         Width           =   8925
         Begin VB.Label Label1 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Item"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   15.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   465
            Left            =   150
            TabIndex        =   5
            Top             =   120
            Width           =   4335
         End
      End
      Begin VB.TextBox txtPayRevaluation 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000001&
         Height          =   360
         Left            =   5700
         Locked          =   -1  'True
         TabIndex        =   3
         Text            =   "1,000,000,000"
         Top             =   1350
         Width           =   1935
      End
      Begin VB.TextBox txtPayAmount 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000001&
         Height          =   360
         Left            =   2910
         TabIndex        =   2
         Text            =   "1,000,000,000"
         Top             =   1350
         Width           =   1515
      End
      Begin VB.TextBox txtCiokKurs 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000001&
         Height          =   360
         Left            =   4500
         TabIndex        =   1
         Text            =   "99,000,000"
         Top             =   1350
         Width           =   1155
      End
      Begin MSComctlLib.ListView lsData 
         Height          =   4635
         Left            =   60
         TabIndex        =   6
         Top             =   2160
         Width           =   7995
         _ExtentX        =   14102
         _ExtentY        =   8176
         View            =   3
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         FlatScrollBar   =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Verdana"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   6
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "No"
            Object.Width           =   882
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Item "
            Object.Width           =   4233
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   2
            Text            =   "Bruto (Gr)"
            Object.Width           =   2646
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   3
            Text            =   "Tukar (%)"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   4
            Text            =   "Revaluasi (Gr)"
            Object.Width           =   4322
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   5
            Text            =   "noUrut"
            Object.Width           =   0
         EndProperty
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Revaluasi (Gr)"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   5700
         TabIndex        =   13
         Top             =   1140
         Width           =   1245
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Tukar (%)"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   4500
         TabIndex        =   12
         Top             =   1110
         Width           =   885
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Bruto  (Gr)"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   2910
         TabIndex        =   11
         Top             =   1110
         Width           =   945
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Item"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   90
         TabIndex        =   10
         Top             =   1080
         Width           =   405
      End
   End
End
Attribute VB_Name = "paymentItemFrm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private rsDumyItemPayment                   As ADODB.Recordset
Private rsItemList                           As ADODB.Recordset

Dim noUrutList                              As Integer


Private Sub cmdAddData_Click()
    If validationInput = True Then
        If cmdAddData.caption = "+" Then
            addToList
        Else
            editList noUrutList
        End If
            
        showList
        zInputConstructor
        
        cboAccountNo.SetFocus
    Else
    
        MsgBox "Lengkapi Inputan Data", vbInformation
        Exit Sub
    End If
End Sub

Private Sub cmdAddTunai_Click()
    paymentGate.itemPaymentSetData rsItemList
    Unload Me
End Sub

Private Sub lsData_DblClick()
    Dim rowSelected                 As Integer
    
    If lsData.ListItems.Count > 0 Then
        rowSelected = lsData.SelectedItem.Index
        With lsData.ListItems(rowSelected)
            noUrutList = Trim(.SubItems(5))
            mappingFromListForEdit noUrutList
        End With
    End If
End Sub

Private Sub txtCiokKurs_Change()
    txtPayRevaluation.Text = Format(hitungNettoFromBruto(val(Format(txtPayAmount.Text, "0.00")), val(Format(txtCiokKurs.Text, "0.0"))), "#,##0.000")
End Sub

Private Sub txtCiokKurs_GotFocus()
    FocusObjek txtCiokKurs
End Sub

Private Sub txtCiokKurs_KeyPress(KeyAscii As Integer)
    bolehInput "7894561230.", KeyAscii
End Sub

Private Sub txtPayAmount_LostFocus()
    txtPayAmount = Format(txtPayAmount.Text, "#,##0.00")
End Sub

Private Sub txtCiokKurs_LostFocus()
    txtCiokKurs = Format(txtCiokKurs.Text, "#,##0.00")
End Sub

Private Sub txtPayAmount_KeyPress(KeyAscii As Integer)
    bolehInput "7894561230.", KeyAscii
End Sub

Private Sub txtPayAmount_Change()
    txtPayRevaluation.Text = Format(hitungNettoFromBruto(val(Format(txtPayAmount.Text, "0.00")), val(Format(txtCiokKurs.Text, "0.0"))), "#,##0.000")
End Sub

Private Sub txtPayAmount_GotFocus()
    FocusObjek txtPayAmount
End Sub

Private Sub cboAccountNo_Change()
    getAccountInfo
End Sub

Private Sub cboAccountNo_Click()
    cboAccountNo_Change
End Sub

Private Sub Form_Load()
    
    cboAccountNo.TabIndex = 0: txtPayAmount.TabIndex = 1: txtCiokKurs.TabIndex = 2: txtPayRevaluation.TabIndex = 3
    cmdAddData.TabIndex = 4: cmdAddTunai.TabIndex = 5: cmdExit.TabIndex = 6
    
    Set rsDumyItemPayment = New ADODB.Recordset
    Set rsItemList = New ADODB.Recordset
    
    rsRekAccInitAtribut
    rsTrfListInitAtribut
    zInputConstructor
End Sub

Private Sub cmdExit_Click()
    Unload Me
End Sub


Public Sub getDataFormPaymentGet(rsData As ADODB.Recordset)
    Set rsItemList = rsData
    showList
End Sub




'====== End Of Commponet Code Line

Private Sub rsRekAccInitAtribut()
    rsDumyItemPayment.Fields.Append "id", adChar, 8
    rsDumyItemPayment.Fields.Append "itemName", adChar, 15
    rsDumyItemPayment.Fields.Append "nilaiTukar", adChar, 15
    rsDumyItemPayment.Open
End Sub

Private Sub rsTrfListInitAtribut()
    rsItemList.Fields.Append "noUrut", adInteger, 3
    rsItemList.Fields.Append "itemId", adChar, 25
    rsItemList.Fields.Append "itemBruto", adDouble, 15
    rsItemList.Fields.Append "itemTukar", adDouble, 7
    rsItemList.Fields.Append "itemNetto", adDouble, 18
    rsItemList.Open
End Sub

Private Sub CreateDummyAccountRek()
    'CIOK
    rsDumyItemPayment.AddNew
    rsDumyItemPayment.Fields("id") = "CIOK"
    rsDumyItemPayment.Fields("itemName") = "CIOK"
    rsDumyItemPayment.Fields("nilaiTukar") = "100"
    rsDumyItemPayment.Update
    
    'LM
    rsDumyItemPayment.AddNew
    rsDumyItemPayment.Fields("id") = "LM"
    rsDumyItemPayment.Fields("itemName") = "LM"
    rsDumyItemPayment.Fields("nilaiTukar") = "100"
    rsDumyItemPayment.Update
    
    'RSK
    rsDumyItemPayment.AddNew
    rsDumyItemPayment.Fields("id") = "RSK" & iCount
    rsDumyItemPayment.Fields("itemName") = "RONGSOK"
    rsDumyItemPayment.Fields("nilaiTukar") = 0
    rsDumyItemPayment.Update
    
    rsDumyItemPayment.MoveFirst
    
End Sub

Private Sub zInputConstructor()
    
    CreateDummyAccountRek
    'Ini Value For Account
    If rsDumyItemPayment.RecordCount > 0 Then
        Do While rsDumyItemPayment.EOF = False
            cboAccountNo.AddItem Trim(rsDumyItemPayment!id)
            rsDumyItemPayment.MoveNext
        Loop
        rsDumyItemPayment.MoveFirst
    End If
    
    cboAccountNo.Text = "-"
    txtAccInfo.Text = "-"
    txtPayAmount.Text = "0.0"
    txtCiokKurs.Text = "0.0"
    txtPayRevaluation.Text = "0.000"
    cmdAddData.caption = "+"
End Sub


Private Sub getAccountInfo()
    If rsDumyItemPayment.RecordCount > 0 Then
        rsDumyItemPayment.MoveFirst
        rsDumyItemPayment.Find ("id='" & Trim(cboAccountNo.Text) & "'")
        If rsDumyItemPayment.EOF = False Then
            txtAccInfo.Text = Trim(rsDumyItemPayment!itemName)
            txtCiokKurs.Text = Format(rsDumyItemPayment!nilaiTukar, "0.00")
        Else
            txtAccInfo.Text = "-"
            txtCiokKurs.Text = 0
        End If
    Else
        txtAccInfo.Text = "-"
        txtCiokKurs.Text = 0
    End If
End Sub

Private Function getNoUrutFromrsTrfList() As Integer
    If rsItemList.RecordCount <= 0 Then
        getNoUrutFromrsTrfList = 1
    Else
        rsItemList.MoveLast
        getNoUrutFromrsTrfList = val(rsItemList!noUrut) + 1
    End If
End Function
    
Private Function validationInput() As Boolean
    validationInput = True
    
    If val(Format(txtPayRevaluation.Text, "0.000")) <= 0 Then
        validationInput = False
    End If
    
    If (txtAccInfo.Text) = "-" Then
        validationInput = False
    End If
End Function
    
Private Sub addToList()
    Dim noUrut                  As Integer
    
    noUrut = val(getNoUrutFromrsTrfList)
    
    rsItemList.AddNew
    rsItemList.Fields("noUrut") = val(noUrut)
    rsItemList.Fields("itemId") = Trim(cboAccountNo.Text)
    rsItemList.Fields("itemBruto") = Format(txtPayAmount.Text, "0.00")
    rsItemList.Fields("itemTukar") = Format(txtCiokKurs.Text, "0.00")
    rsItemList.Fields("itemNetto") = Format(txtPayRevaluation.Text, "0.000")
    rsItemList.Update
End Sub

Private Sub editList(noUrut As Integer)
    
    rsItemList.MoveFirst
    rsItemList.Find ("noUrut='" & Trim(noUrut) & "'")
    
    If rsItemList.EOF = False Then
        rsItemList.Fields("noUrut") = val(noUrut)
        rsItemList.Fields("itemId") = Trim(cboAccountNo.Text)
        rsItemList.Fields("itemBruto") = Format(txtPayAmount.Text, "0.00")
        rsItemList.Fields("itemTukar") = Format(txtCiokKurs.Text, "0.00")
        rsItemList.Fields("itemNetto") = Format(txtPayRevaluation.Text, "0.000")
        rsItemList.Update
    End If
End Sub

Private Sub showList()
    Dim iCounter                        As Integer
    
    rsItemList.MoveFirst
    lsData.ListItems.Clear
    Do While Not rsItemList.EOF
        iCounter = iCounter + 1
        lsData.ListItems.Add , , val(iCounter)
        lsData.ListItems(iCounter).SubItems(1) = Trim(rsItemList!itemId)
        lsData.ListItems(iCounter).SubItems(2) = Format(rsItemList!itemBruto, "#,##0.00")
        lsData.ListItems(iCounter).SubItems(3) = Format(rsItemList!itemTukar, "#,##0.00")
        lsData.ListItems(iCounter).SubItems(4) = Format(rsItemList!itemNetto, "#,##0.000")
        lsData.ListItems(iCounter).SubItems(5) = val(rsItemList!noUrut)
        rsItemList.MoveNext
    Loop
End Sub

Private Sub mappingFromListForEdit(noUrut As Integer)
    rsItemList.MoveFirst
    rsItemList.Find ("noUrut='" & Trim(noUrut) & "'")
    
    If rsItemList.EOF = False Then
        cboAccountNo.Text = Trim(rsItemList.Fields("itemId"))
        txtPayAmount.Text = Format(rsItemList.Fields("itemBruto"), "#,##0.00")
        txtCiokKurs.Text = Format(rsItemList.Fields("itemTukar"), "0.00")
        
        cmdAddData.caption = "!"
    End If
End Sub
