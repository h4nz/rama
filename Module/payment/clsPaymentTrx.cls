VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsPaymentTrx"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Private conData                 As connection

Private rsTunai                 As ADODB.Recordset
Private rsTrfList               As ADODB.Recordset
Private rsItemList              As ADODB.Recordset
Private rsSummary               As ADODB.Recordset

Private paymentTbl              As String
Private paymentTblDeatil        As String


Private Sub Class_Initialize()
    Set rsTunai = New ADODB.Recordset
    Set rsTrfList = New ADODB.Recordset
    Set rsItemList = New ADODB.Recordset
    Set rsSummary = New ADODB.Recordset
    
    paymentTbl = "td_payment"
    paymentTblDeatil = "td_payment_detail"
    
    Set conData = New connection
    conData.openConnection
End Sub

Public Function savePaymentTrx(dataSummary As ADODB.Recordset, paymentCash As ADODB.Recordset, paymentTrf As ADODB.Recordset, paymentItem As ADODB.Recordset) As Boolean
    Dim statusProcess               As Boolean
    
    Set rsSummary = dataSummary
    Set rsTunai = paymentCash
    Set rsTrfList = paymentTrf
    Set rsItemList = paymentItem
    
    statusProcess = savePayment
    savePaymentTrx = statusProcess
End Function

Private Function savePayment() As Boolean
    Dim idTransaction                       As String
    Dim piutangSaldo                        As clsPiutangTrx
    Dim piutangLogPayLoad                   As String
    Dim piutangSldPayLoad                   As ADODB.Recordset
    
    Set piutangSaldo = New clsPiutangTrx
    idTransaction = GetGUID
    
    conData.openConnection
    conData.objConn.BeginTrans

On Error GoTo savePayment_Error
    savePaymentDetail idTransaction
    savePaymentHead idTransaction
    
    With rsSummary
        rsSummary.MoveFirst
        If .RecordCount > 0 Then
            piutangSaldo.substractPiutangCustByFifo Format(Now, "YYYY-MM-DD"), idTransaction, Trim(!idCust), val(Format(!totalPayment, "0.000")), 0, "PEMBAYARAN-CUST"
        End If
    End With
    
    piutangLogPayLoad = piutangSaldo.payLoadPiutangLogBatch
    Set piutangSldPayLoad = piutangSaldo.payLoadPiutangSaldoBatch
    
    saveProcessPiutang piutangLogPayLoad, piutangSldPayLoad
    
    conData.objConn.CommitTrans
    savePayment = True
    
exit_save_payment:
    conData.closeConnection
    
    Set piutangSaldo = Nothing
    Exit Function


savePayment_Error:
    conData.objConn.RollbackTrans
    savePayment = False
    Resume exit_save_payment
    Exit Function

End Function

Private Sub savePaymentHead(id As String)
    Dim payLoad                 As String
    Dim referenceNo             As String
    
    referenceNo = "-"
    If rsSummary.RecordCount > 0 Then
        rsSummary.MoveFirst
        Do While rsSummary.EOF = False
            payLoad = "INSERT INTO " & Trim(paymentTbl) & " (id,tanggal,reference_no,id_customer,sum_cash_amount,sum_cash_netto,sum_trf_amount,sum_trf_netto, " & _
                      "sum_item_bruto,sum_item_netto,sum_payment_netto,created_by,created_date) VALUES ( " & _
                      "'" & Trim(id) & "','" & Format(Now, "YYYY-MM-DD") & "','" & Trim(referenceNo) & "','" & Trim(rsSummary!idCust) & "' " & _
                      ",'" & val(Format(rsSummary!cashAmount, "0.0")) & "','" & val(Format(rsSummary!cashnetto, "0.000")) & "'" & _
                      ",'" & val(Format(rsSummary!transferAmount, "0.0")) & "','" & val(Format(rsSummary!transferNetto, "0.000")) & "'" & _
                      ",'" & val(Format(rsSummary!itemAmount, "0.00")) & "','" & val(Format(rsSummary!itemNetto, "0.000")) & "'" & _
                      ",'" & val(Format(rsSummary!totalPayment, "0.00")) & "'" & _
                      ",'" & Trim(getCreatedBy) & "','" & Format(Now, "YYYY-MM-DD HH:MM:SS") & "'" & _
                      ");" & vbCrLf
            
            rsSummary.MoveNext
        Loop
        
    End If
    
    conData.queryExecutionForTransaction payLoad
    
End Sub

Private Sub savePaymentDetail(id As String)
    Dim payLoad                 As String
    Dim tblColumn               As String
    Dim rowNo                   As String
    
    'Metode Menggunakan Insert Bulk
    '1. Siapkan Full layout tabledetail nya
    tblColumn = "INSERT INTO " & Trim(paymentTblDeatil) & " ( " & _
                "id,row_no,cash_amount,cash_ciok_kurs,cash_amount_netto " & _
                ",no_rek_account,transfer_amount,transfer_ciok_kurs,transfer_amount_netto " & _
                ",item_id,item_bruto_weight,item_tukar,item_netto_weight" & _
                ") VALUES "
    
    
    'Write For Tunai Payment Method
    If rsTunai.RecordCount > 0 Then
        rsTunai.MoveFirst
        With rsTunai
            Do While rsTunai.EOF = False
                rowNo = val(rowNo) + 1
                payLoad = "( " & _
                          "'" & Trim(id) & "','" & val(rowNo) & "','" & Format(!totalTunai, "0.0") & "','" & Format(!ciok, "0.0") & "','" & Format(!nettTunai, "0.000") & "','-','0.00','0.00','0.000','-','0.00','0.00','0.000'" & _
                          ")"
                .MoveNext
                
            Loop
        End With
    End If
    
    'Write For Transfer Payment Method
    Dim rowTrf                          As Integer
    
    If rsTrfList.RecordCount > 0 Then
        With rsTrfList
            rsTrfList.MoveFirst
            Do While rsTrfList.EOF = False
                rowNo = val(rowNo) + 1
                
                If Len(payLoad) <= 0 Then
                    payLoad = "(" & _
                          "'" & Trim(id) & "','" & val(rowNo) & "' " & _
                          ",'0.0','0.0','0.000' " & _
                          ",'" & Trim(!accountNumber) & "','" & Format(!paymentAmount, "0.0") & "','" & Format(!ciokKurs, "0.0") & "','" & Format(!revaluationAmount, "0.000") & "'" & _
                          ",'-','0.00','0.00','0.000' " & _
                          ")"
                Else
                    payLoad = Trim(payLoad) & ",( " & _
                          "'" & Trim(id) & "','" & val(rowNo) & "' " & _
                          ",'0.0','0.0','0.000' " & _
                          ",'" & Trim(!accountNumber) & "','" & Format(!paymentAmount, "0.0") & "','" & Format(!ciokKurs, "0.0") & "','" & Format(!revaluationAmount, "0.000") & "'" & _
                          ",'-','0.00','0.00','0.000' " & _
                          ")"
                End If
                
                .MoveNext
                
            Loop
        End With
    End If
    
    'Write For Item Payment Method
    'For A while item structur same with transfer
    If rsItemList.RecordCount > 0 Then
        With rsItemList
            rsItemList.MoveFirst
            Do While rsItemList.EOF = False
                rowNo = val(rowNo) + 1
                If Len(payLoad) <= 0 Then
                    payLoad = "( " & _
                              "'" & Trim(id) & "','" & val(rowNo) & "' " & _
                              ",'0.0','0.0','0.000' " & _
                              ",'-','0.0','0.0','0.000' " & _
                              ",'" & Trim(!itemId) & "','" & Format(!itemBruto, "0.000") & "','" & Format(!itemTukar, "0.00") & "','" & Format(!itemNetto, "0.000") & "'" & _
                              ")"
                Else
                    payLoad = payLoad & ",( " & _
                              "'" & Trim(id) & "','" & val(rowNo) & "' " & _
                              ",'0.0','0.0','0.000' " & _
                              ",'-','0.0','0.0','0.000' " & _
                              ",'" & Trim(!itemId) & "','" & Format(!itemBruto, "0.000") & "','" & Format(!itemTukar, "0.00") & "','" & Format(!itemNetto, "0.000") & "'" & _
                              ")"
                End If
                
                .MoveNext
            Loop
        End With
    End If
    
    payLoad = tblColumn & payLoad
    conData.bulkQueryExecutionWithCommand payLoad
End Sub

Private Sub saveProcessPiutang(payloadPiutangLog As String, payloadPiutangSaldo As ADODB.Recordset)
    'For Execute Piutang Log Batch
    conData.bulkQueryExecutionWithCommand payloadPiutangLog
    
    'For Execute Piutang Saldo Batch Update
    If payloadPiutangSaldo.RecordCount > 0 Then
        With payloadPiutangSaldo
            .MoveFirst
            Do While .EOF = False
                conData.queryExecutionForTransaction Trim(!value)
                payloadPiutangSaldo.MoveNext
            Loop
        End With
    End If
End Sub
