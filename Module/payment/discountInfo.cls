VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "discountInfo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Dim piutangInfo                         As clsPiutangInfo
Dim pjlInfoDetailLst                    As clsPjlInformation
Dim rsPiutangList                       As New ADODB.Recordset
Dim rsPjlInfoDiscList                   As New ADODB.Recordset

Dim discountLimitDay                    As Integer
Dim rsPiutangDiscountList               As ADODB.Recordset

Public Property Get discLimitDay() As Integer
    discLimitDay = discountLimitDay
End Property

Public Property Let discLimitDay(vLimit As Integer)
    discountLimitDay = vLimit
End Property

Private Sub Class_Initialize()
    
    Set piutangInfo = New clsPiutangInfo
    Set pjlInfoDetailLst = New clsPjlInformation
    discLimitDay = 10  ' This is default hardcoded
    
End Sub

Public Function getDiscAllByIdCust(idCust As String) As ADODB.Recordset
    getDataPiutangByCustId idCust
End Function

Public Property Get discListDetail() As ADODB.Recordset
    
    If rsPiutangDiscountList.RecordCount > 0 Then
        Set discListDetail = rsPiutangDiscountList
    Else
        Set discListDetail = Nothing
    End If
End Property

















'Batas Private Method
'==============================================

Private Sub cunstructPiutangDiscList()
    
    Set rsPiutangDiscountList = New ADODB.Recordset
    
    rsPiutangDiscountList.Fields.Append "dateTrx", adDate
    rsPiutangDiscountList.Fields.Append "noFaktur", adChar, 35
    rsPiutangDiscountList.Fields.Append "idFaktur", adChar, 35
    rsPiutangDiscountList.Fields.Append "idItem", adChar, 15
    rsPiutangDiscountList.Fields.Append "bruto", adDouble, 10
    rsPiutangDiscountList.Fields.Append "discPersen", adDouble, 10
    rsPiutangDiscountList.Fields.Append "discNetto", adDouble, 15
    rsPiutangDiscountList.Open
End Sub

Private Sub addPiutangList(dateTrx As Date, noFaktur As String, idFaktur As String, idItem As String, bruto As Double, discPersen As Double, discNetto As Double)
    rsPiutangDiscountList.AddNew
    rsPiutangDiscountList.Fields("dateTrx") = Format(dateTrx, "YYYY-MM-DD")
    rsPiutangDiscountList.Fields("noFaktur") = Trim(noFaktur)
    rsPiutangDiscountList.Fields("idFaktur") = Trim(idFaktur)
    rsPiutangDiscountList.Fields("idItem") = Trim(idItem)
    rsPiutangDiscountList.Fields("bruto") = Format(bruto, "0.000")
    rsPiutangDiscountList.Fields("discPersen") = Format(discPersen, "0.00")
    rsPiutangDiscountList.Fields("discNetto") = Format(discNetto, "0.000")
    rsPiutangDiscountList.Update
End Sub

Private Sub cunstructPjlInfoDiscList()
    rsPjlInfoDiscList.Fields.Append "idBarang", adDate
    rsPjlInfoDiscList.Fields.Append "bruto", adDate
    rsPjlInfoDiscList.Fields.Append "disc", adDate
    rsPjlInfoDiscList.Fields.Append "nettoDisc", adDate
    rsPjlInfoDiscList.Open
End Sub

Private Sub getDataPiutangByCustId(idCust As String)
    Set rsPiutangList = Nothing
    Set rsPiutangList = New ADODB.Recordset
    piutangInfo.getSaldoByIdCust Trim$(idCust), 0
    Set rsPiutangList = piutangInfo.getSaldoInfoList
    
    If rsPiutangList.EOF = True Then
        Set rsPiutangList = Nothing
        Set rsPiutangList = New ADODB.Recordset
        
        Set rsPiutangDiscountList = Nothing
    Else
        calculatePiutangDiscount
    End If
    
End Sub

Private Sub calculatePiutangDiscount()
    
    Set rsPiutangDiscountList = Nothing
    cunstructPiutangDiscList
    If rsPiutangList.EOF = False And rsPiutangList.RecordCount > 0 Then
        rsPiutangList.MoveFirst
        
        With rsPiutangList
            Do While .EOF = False
                
                Set rsPjlInfoDiscList = Nothing
                cunstructPjlInfoDiscList
                
                'This is count date diff betweet trx date from saldo data and now(Date)
                'If (DateDiff("d", Format(!tanggal, "YYYY-MM-DD"), Format(nowDate, "YYYY-MM-DD"))) <= discountLimitDay Then
                    getPenjualanDetailItemById Trim$(!id)
                    If rsPjlInfoDiscList.RecordCount > 0 Then
                        rsPjlInfoDiscList.MoveFirst
                        Do While rsPjlInfoDiscList.EOF = False
                            addPiutangList Format(!tanggal, "YYYY-MM-DD"), Trim$(!id), Trim$(!id), Trim$(rsPjlInfoDiscList!idBarang), val(Format(rsPjlInfoDiscList!bruto, "0.000")), Format(rsPjlInfoDiscList!disc) _
                            , Format(hitungNettoFromBruto(val(Format(rsPjlInfoDiscList!bruto, "0.000")), val(Format(rsPjlInfoDiscList!disc, "0.00"))), "0.000")
                            
                            rsPjlInfoDiscList.MoveNext
                        Loop
                    End If
                'End If
                
                .MoveNext
            Loop
        End With
    End If
    
    Set rsPjlInfoDiscList = Nothing
End Sub

Private Sub getPenjualanDetailItemById(idFaktur As String)
    Set rsPjlInfoDiscList = pjlInfoDetailLst.findTrxDetailWithDiscInfo_1(Trim(idFaktur))
    
    If rsPjlInfoDiscList.EOF = True Then
        Set rsPjlInfoDiscList = Nothing
        cunstructPjlInfoDiscList
    Else
        rsPjlInfoDiscList.MoveFirst
    End If
    
End Sub
