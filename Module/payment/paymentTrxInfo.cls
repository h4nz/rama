VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clspaymentTrxInfo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Private conData                 As connection
Private payloadRequest          As ADODB.Recordset
Private rsResultTmp             As ADODB.Recordset
Private rsPrintRecipt           As ADODB.Recordset

Private paymentTbl              As String
Private paymentTblDeatil        As String
Private tmpResultFile           As String

Private SetPrinter              As New ntPrinter

Public Enum resultDataMethod
    DataPaymentSum = 0
    paymentPrintRecipt = 1
End Enum

Private Sub Class_Initialize()
    paymentTbl = "td_payment"
    paymentTblDeatil = "td_payment_detail"
    tmpResultFile = App.Path
    
    Set conData = New connection
    conData.openConnection
    
    Set payloadRequest = New ADODB.Recordset
    constructRequestPayload
End Sub

Public Sub setNothinResultTmp()
    Set rsResultTmp = Nothing
End Sub

Public Function getPayloadDto() As ADODB.Recordset
    Set getPayloadDto = payloadRequest
End Function

Public Sub setPayloadRequestValue(rsData As ADODB.Recordset)
    payloadRequest.Fields("dateFrom") = Format(rsData!dateFrom, "YYYY-MM-DD")
    payloadRequest.Fields("dateTo") = Format(rsData!dateTo, "YYYY-MM-DD")
    payloadRequest.Fields("idCust") = Trim(rsData!idCust)
    payloadRequest.Update
End Sub
    
Public Function processGetDataPaymentSum() As ADODB.Recordset
     If (getDataPaymentSum) = True Then
        opeRsFromLocal (DataPaymentSum)
        Set processGetDataPaymentSum = rsResultTmp
     Else
        Set processGetDataPaymentSum = rsResultTmp
     End If
End Function

Public Sub paymentTrxPrintRecipt(id As String)
    processPaymentInfoById id
    mappingPaymentPrintRecipt
    ShowPaymentPrintRecipt
End Sub




Private Sub constructRequestPayload()
    payloadRequest.Fields.Append "dateFrom", adDate
    payloadRequest.Fields.Append "dateTo", adDate
    payloadRequest.Fields.Append "idCust", adChar, 15
    payloadRequest.Open
    
    payloadRequest.AddNew
    payloadRequest.Fields("dateFrom") = Format(Now, "YYYY-MM-DD")
    payloadRequest.Fields("dateTo") = Format(Now, "YYYY-MM-DD")
    payloadRequest.Fields("idCust") = "-"
    payloadRequest.Update
End Sub

Private Sub constructRsPrintRecipt()
    Set rsPrintRecipt = New ADODB.Recordset
    rsPrintRecipt.Fields.Append "no_urut", adInteger, 5
    rsPrintRecipt.Fields.Append "id_barang", adChar, 25
    rsPrintRecipt.Fields.Append "bruto", adChar, 15
    rsPrintRecipt.Fields.Append "kurs", adChar, 15
    rsPrintRecipt.Fields.Append "netto", adDouble, 15
    rsPrintRecipt.Open
End Sub

Private Sub nothingRsPrintRecipt()
    rsPrintRecipt.Close
    Set rsPrintRecipt = Nothing
End Sub

Private Function getDataPaymentSum() As Boolean

    Dim rsData                  As New ADODB.Recordset
    Dim payloadQuery            As String
    Dim payloadFilter           As String
    
    payloadFilter = "WHERE (a.tanggal BETWEEN '" & Format(payloadRequest!dateFrom, "YYYY-MM-DD") & "' AND '" & Format(payloadRequest!dateTo, "YYYY-MM-DD") & "' )"
    If Len(Trim(payloadRequest!idCust)) > 0 And Trim(payloadRequest!idCust) <> "-" Then
        payloadFilter = payloadFilter & " AND a.id_customer ='" & Trim(payloadRequest!idCust) & "'"
    End If
    
    payloadQuery = "SELECT a.tanggal,concat(b.nama,'-',b.alamat) as cust_info,a.sum_cash_amount,a.sum_trf_amount,a.sum_item_bruto,a.sum_payment_netto,a.created_date,a.id  " & _
                   "FROM td_payment A JOIN md_customer B ON a.id_customer=b.id " & payloadFilter & _
                   ";"
    
    Set rsData = conData.queryExecution(payloadQuery)
    If rsData.EOF = False Then
        saveRsToLocal rsData, DataPaymentSum
        getDataPaymentSum = True
    Else
        getDataPaymentSum = False
    End If
    Set rsData = Nothing
    conData.closeConnection
End Function

Private Function getFileName(tipe As resultDataMethod) As String
    getFileName = "resultTmp.adt"
    Select Case tipe
           Case DataPaymentSum
                getFileName = "DataPaymentSum.adt"
           Case paymentPrintRecipt
                getFileName = "paymentPrintRecipt.adt"
    End Select
End Function

Private Sub saveRsToLocal(rsData As ADODB.Recordset, tipe As resultDataMethod)
     
    On Error GoTo FileAlreadyExist
    If rsData.EOF = False Then
saveData:
        rsData.Save tmpResultFile & "\" & Trim(getFileName(tipe)), adPersistADTG
    End If

Exit Sub

FileAlreadyExist:
    If Err.Number = 58 Then
        Kill tmpResultFile & "\" & Trim(getFileName(tipe))
        Resume saveData
    End If
    'Debug.Print Err.Description
    'Debug.Print Err.Number
Exit Sub
End Sub

Private Function opeRsFromLocal(tipe As resultDataMethod)

On Error GoTo FileNotFound
    Set rsResultTmp = New ADODB.Recordset
    With rsResultTmp
        .Open tmpResultFile & "\" & Trim(getFileName(tipe)), , , , adCmdFile
    End With

Exit Function
FileNotFound:
    If Err.Number = -2147287038 Then
        'File Not Found
       Set rsResultTmp = Nothing
       Exit Function
    End If
    'Debug.Print Err.Description
    'Debug.Print Err.Number
End Function


Private Function processPaymentInfoById(id As String) As Boolean
    Dim rsData                      As ADODB.Recordset
    Dim Query                       As String
    
    Query = "SELECT b.tanggal,CONCAT(c.nama,'', c.alamat) as cust_info,a.* FROM td_payment_detail A JOIN td_payment B ON a.id = b.id JOIN md_customer C ON b.id_customer = c.id  " & _
            "WHERE b.id = '" & Trim(id) & "';"
    Set rsData = conData.queryExecution(Query)
    If rsData.EOF = False Then
        saveRsToLocal rsData, paymentPrintRecipt
        processPaymentInfoById = True
    Else
        processPaymentInfoById = False
    End If
    Set rsData = Nothing
    conData.closeConnection
End Function

Private Sub mappingPaymentPrintRecipt()
    Dim rsData                      As ADODB.Recordset
    
    opeRsFromLocal paymentPrintRecipt
    Set rsData = rsResultTmp
    rsData.Sort = "row_no"
    If rsData.EOF = False Then
        constructRsPrintRecipt
        rsData.MoveFirst
        
        Do While rsData.EOF = False
            If val(Format(rsData!cash_amount, "0.0")) > 0 Then
                mappingDataPaymentPrintRecipt val(rsData!row_no), "RP", "Tunai", val(Format(rsData!cash_amount, "0")), val(Format(rsData!cash_ciok_kurs, "0")), val(Format(rsData!cash_amount_netto, "0.000"))
            End If
            
            If Trim(rsData!no_rek_account) <> "-" Then
                mappingDataPaymentPrintRecipt val(rsData!row_no), "RP", "Trf-" & Trim(rsData!no_rek_account), val(Format(rsData!transfer_amount, "0")), val(Format(rsData!transfer_ciok_kurs, "0")), val(Format(rsData!transfer_amount_netto, "0.000"))
            End If
            
            If Trim(rsData!item_id) <> "-" Then
                mappingDataPaymentPrintRecipt val(rsData!row_no), "BRUTO", Trim(rsData!item_id), val(Format(rsData!item_bruto_weight, "0.000")), val(Format(rsData!item_tukar, "0.00")), val(Format(rsData!item_netto_weight, "0.000"))
            End If
            
            rsData.MoveNext
        Loop
    End If
    Set rsData = Nothing
    
End Sub

Private Sub mappingDataPaymentPrintRecipt(noUrt As Integer, paymentCategory As String, itemPayment As String, val As Double, kurs As Double, nett As Double)
    rsPrintRecipt.AddNew
    rsPrintRecipt.Fields("no_urut") = noUrt
    rsPrintRecipt.Fields("id_barang") = Trim(itemPayment)
    If (paymentCategory) = "RP" Then
        rsPrintRecipt.Fields("bruto") = Format(val, "#,##0")
        rsPrintRecipt.Fields("kurs") = Format(kurs, "#,##0")
    Else
        rsPrintRecipt.Fields("bruto") = Format(val, "#,##0.00")
        rsPrintRecipt.Fields("kurs") = Format(kurs, "#,##0.00")
    End If
    rsPrintRecipt.Fields("netto") = Format(nett, "#,##0.000")
    rsPrintRecipt.Update
End Sub

Private Sub ShowPaymentPrintRecipt()
    Dim rsData                      As New ADODB.Recordset
        
    opeRsFromLocal paymentPrintRecipt
    Set rsData = rsResultTmp
    SetPrinter.SetPrinterOrientation ntPortrait, vbPRPSA4, "LAPORAN"
    With PaymentPrintReciptRpt
        Set .DataSource = rsPrintRecipt
            .Sections("Section2").Controls("transactionDate").caption = Format(rsData!tanggal, "DD-MM-YYYY")
            .Sections("Section2").Controls("transactionId").caption = Trim(rsData!id)
            .Sections("Section2").Controls("customerInformation").caption = Trim(rsData!cust_info)
        
        .Show
    End With
End Sub
