VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsCst"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
 
 Dim tblName                    As String
 Dim conData                    As connection


Private Sub Class_Initialize()
    Set conData = New connection
    tblName = "md_customer"
End Sub


Public Function getCustInfo(custId As String) As Collection
    Dim RS              As New ADODB.Recordset
    Dim clData          As Collection
    
    Set clData = New Collection
    Set RS = New ADODB.Recordset
    
    conData.openConnection
    Set RS = conData.queryExecution("SELECT * FROM " & tblName & " WHERE id = '" & Trim(custId) & "' ")
    If RS.EOF = False Then
        clData.Add Trim(RS!id)
        clData.Add "Nama   : " & Trim(RS!nama)
        clData.Add "Alamat : " & Trim(RS!alamat)
        clData.Add "Tlfn   : " & Trim(RS!telephone)
    End If
    
    conData.closeConnection
    Set getCustInfo = clData
End Function
