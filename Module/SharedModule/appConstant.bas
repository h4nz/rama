Attribute VB_Name = "appConstant"
Option Explicit

Private Type GUID
Data1 As Long
Data2 As Integer
Data3 As Integer
Data4(7) As Byte
End Type

Private Declare Function CoCreateGuid Lib "OLE32.DLL" (pGuid As GUID) As Long
Public Declare Function SetParent Lib "user32" (ByVal hWndChild As Long, ByVal hWndNewParent As Long) As Long
Public Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Long)

Dim createdBy               As String
Dim publicDate              As Date




Public Function getCreatedBy() As String
        getCreatedBy = "Admin"
End Function

Public Property Get nowDate() As Date
    nowDate = publicDate
End Property

Public Property Let nowDate(vDate As Date)
    publicDate = Format(vDate, "YYYY-MM-DD")
End Property






Public Function hitungNettoFromBruto(bruto As Double, tukar As Double) As Double
    
    If bruto = 0 Or tukar = 0 Then
        hitungNettoFromBruto = 0
    Else
        hitungNettoFromBruto = val(bruto * tukar) / 100
    End If
End Function

Public Function hitungNettoFromRp(amount As Double, ciok As Double) As Double
    
    If amount = 0 Or ciok = 0 Then
        hitungNettoFromRp = 0
    Else
        hitungNettoFromRp = val(Format(amount, "0.0")) / val(Format(ciok, "0.0"))
    End If
End Function

Public Function GetGUID() As String
Dim udtGUID         As GUID

    If (CoCreateGuid(udtGUID) = 0) Then
        Debug.Print udtGUID.Data1
        Debug.Print udtGUID.Data2
        Debug.Print udtGUID.Data3
        
        GetGUID = _
            String(8 - Len(Hex$(udtGUID.Data1)), "0") & Hex$(udtGUID.Data1) & _
            String(4 - Len(Hex$(udtGUID.Data2)), "0") & Hex$(udtGUID.Data2) & _
            String(4 - Len(Hex$(udtGUID.Data3)), "0") & Hex$(udtGUID.Data3) & _
            IIf((udtGUID.Data4(0) < &H10), "0", "") & Hex$(udtGUID.Data4(0)) & _
            IIf((udtGUID.Data4(1) < &H10), "0", "") & Hex$(udtGUID.Data4(1)) & _
            IIf((udtGUID.Data4(2) < &H10), "0", "") & Hex$(udtGUID.Data4(2)) & _
            IIf((udtGUID.Data4(3) < &H10), "0", "") & Hex$(udtGUID.Data4(3)) & _
            IIf((udtGUID.Data4(4) < &H10), "0", "") & Hex$(udtGUID.Data4(4)) & _
            IIf((udtGUID.Data4(5) < &H10), "0", "") & Hex$(udtGUID.Data4(5)) & _
            IIf((udtGUID.Data4(6) < &H10), "0", "") & Hex$(udtGUID.Data4(6)) & _
            IIf((udtGUID.Data4(7) < &H10), "0", "") & Hex$(udtGUID.Data4(7))
    
    End If
End Function

Public Sub bolehInput(ValidCharacter As String, KeyAscii As Integer)
    If KeyAscii > 26 Then
        If InStr(ValidCharacter, Chr(KeyAscii)) = 0 Then
            KeyAscii = 0
            Beep
        End If
    End If
End Sub

Public Sub janganInput(ValidCharacter As String, KeyAscii As Integer)
    If KeyAscii > 26 Then
        If InStr(ValidCharacter, Chr(KeyAscii)) <> 0 Then
            KeyAscii = 0
            Beep
        End If
    End If
End Sub

Sub FocusObjek(objek As Object)
    objek.SelStart = 0
    objek.SelLength = Len(objek)
End Sub

Sub CurrencyAmountInputFormat(object As TextBox)
    object.Text = Format(object.Text, "#,##0")
    object.SelStart = Len(object.Text) + 1
End Sub

Public Sub HideColumntLv(Lv As ListView, Col As Integer)
    Dim IntColumn           As Integer
    
    For IntColumn = val(Col) + val(1) To Lv.ColumnHeaders.Count
        Lv.ColumnHeaders(IntColumn).Width = 0
    Next IntColumn
    
End Sub
