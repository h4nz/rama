VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "connection"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Public objConn         As New ADODB.connection
Dim Command            As New ADODB.Command


Public Sub openConnection()
    Dim str         As String
    
    On Error GoTo Err_Exception
    If objConn.State Then Exit Sub
        
    str = "gConnect"
    Set objConn = New ADODB.connection
    objConn.ConnectionTimeout = 3
    objConn.Open str
    DBServer.CursorLocation = adUseClient
    
    Exit Sub
Err_Exception:
    If Err.Number = "-2147467259" Then
        MsgBox "DATABASE SERVER Tidak Ditemukan, PERIKSA SETTINGAN DATABASE ANDA!", vbCritical + vbOKOnly, "G"
        End
    End If
End Sub

Public Sub closeConnection()
    objConn.Close
End Sub

Public Function queryExecution(payLoad As String) As ADODB.Recordset
    openConnection
    Set queryExecution = objConn.Execute(payLoad)
End Function

Public Function queryExecutionForTransaction(payLoad As String) As ADODB.Recordset
    objConn.Execute (payLoad)
End Function

Public Function bulkQueryExecutionWithCommand(payLoad As String) As ADODB.Recordset
    If Len(payLoad) <= 0 Then Exit Function
    With Command
         .ActiveConnection = objConn
         .CommandType = adCmdText
         .CommandText = payLoad
         .Execute
    End With
    Set Command = Nothing
End Function
