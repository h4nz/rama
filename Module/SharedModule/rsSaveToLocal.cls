VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "rsSaveToLocal"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Private rsResultTmp             As ADODB.Recordset
Private tmpResultFile           As String


Private Sub Class_Initialize()
    tmpResultFile = App.Path
End Sub

Public Function getRsResult() As ADODB.Recordset
    Set getRsResult = rsResultTmp
End Function


Public Sub saveRsToLocal(rsData As ADODB.Recordset, fileName As String)
     
    On Error GoTo FileAlreadyExist
    fileName = fileName & ".adt"
saveData:
    rsData.Save tmpResultFile & "\" & Trim(fileName), adPersistADTG
    

Exit Sub

FileAlreadyExist:
    If Err.Number = 58 Then
        Kill tmpResultFile & "\" & Trim(fileName)
        Resume saveData
    End If
Exit Sub
End Sub

Public Function opeRsFromLocal(fileName As String)

On Error GoTo FileNotFound
    fileName = fileName & ".adt"
    Set rsResultTmp = New ADODB.Recordset
    With rsResultTmp
        .Open tmpResultFile & "\" & Trim(fileName), , , , adCmdFile
    End With

Exit Function
FileNotFound:
    If Err.Number = -2147287038 Then
        'File Not Found
       Set rsResultTmp = Nothing
       Exit Function
    End If
End Function

Private Sub removeFile(fileName As String)

On Error GoTo FileNotFound
    Kill tmpResultFile & "\" & Trim(fileName)
    
Exit Sub
FileNotFound:
    If Err.Number = -2147287038 Then
        'File Not Found
       Set rsResultTmp = Nothing
       Exit Sub
    End If
End Sub
