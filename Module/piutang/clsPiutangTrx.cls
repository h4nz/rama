VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsPiutangTrx"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'1. Piutang log
'2 Piutang Saldo

Option Explicit

Private conData                          As connection
Private piutangInfo                      As clsPiutangInfo
Private transactionLogTable              As String
Private trxSaldoPiutangTable             As String

Private payloadPiutangLogInsBatch       As String
Private payloadPiutangSaldoUpdtBatch    As New ADODB.Recordset


Public Enum transactionType
    debit = 0
    credit = 1
End Enum

Private Sub Class_Initialize()
    Set conData = New connection
    conData.openConnection
    
    Set piutangInfo = New clsPiutangInfo
    
    transactionLogTable = "tl_piutang"
    trxSaldoPiutangTable = "ts_piutang"
End Sub

Private Sub Class_Terminate()
    conData.closeConnection
    Set conData = Nothing
    Set payloadPiutangSaldoUpdtBatch = Nothing
End Sub

Public Property Let payLoadPiutangLogBatch(vData As String)
    payloadPiutangLogInsBatch = vData
End Property

Public Property Get payLoadPiutangLogBatch() As String
    payLoadPiutangLogBatch = payloadPiutangLogInsBatch
End Property

Public Property Let payLoadPiutangSaldoBatch(vData As ADODB.Recordset)
    Set payloadPiutangSaldoUpdtBatch = vData
End Property

Public Property Get payLoadPiutangSaldoBatch() As ADODB.Recordset
    Set payLoadPiutangSaldoBatch = payloadPiutangSaldoUpdtBatch
End Property

Public Function writeNewPiutangSaldo(field As String, tgl As Date, idCust As String, idTrx As String, nett As Double, rp As Double) As String
    Dim payLoad                 As String
    
    payLoad = "INSERT INTO " & Trim(trxSaldoPiutangTable) & "(tanggal,id_customer,id_penjualan," & Trim(field) & "_netto," & Trim(field) & "_rp,akhir_netto,akhir_rp) VALUES ( " & _
                           "'" & Format(tgl, "YYYY-MM-DD") & "','" & Trim(idCust) & "','" & Trim(idTrx) & "'" & _
                           ",'" & Format(nett, "0.000") & "','" & Format(rp, "0.0") & "'" & _
                           ",'" & Format(nett, "0.000") & "','" & Format(rp, "0.0") & "'" & _
                           ") "
    
    writeNewPiutangSaldo = payLoad
End Function

Public Function savePiutangLog(tgl As Date, trxTtpe As transactionType, idPenjualan As String, nett As Double, rp As Double, noref As String, ket As String) As String
    Dim savePayload                 As String
    Dim getId                       As String
    Dim getTrxType                  As String
    
    DoEvents
    getTrxType = getTransactionType(trxTtpe)
    
    DoEvents
    savePayload = "INSERT INTO " & Trim(transactionLogTable) & "(tanggal,id_penjualan,transaction_type,netto,rp,no_reference,keterangan,created_by,created_date) VALUES  ( " & _
                  "'" & Format(tgl, "YYYY-MM-DD") & "','" & Trim(idPenjualan) & "','" & Trim(getTrxType) & "','" & Format(nett, "0.000") & "','" & Format(rp, "0.0") & "' " & _
                  ",'" & Trim(noref) & "','" & Trim(ket) & "','" & Trim(getCreatedBy) & "','" & Format(Now, "YYYY-MM-DD HH:MM:SS") & "'" & _
                  ") "
                  
    savePiutangLog = savePayload
End Function

Public Function substractPiutangCustByFifo(tglFaktur As Date, idReff As String, idCust As String, payAmountNett As Double, payAmountRp As Double, ket As String) As Boolean
    Dim rsPiutangSaldo                          As ADODB.Recordset
    Dim payAmountBalance                        As Double
    Dim valAmount                               As Double
    Dim piutangLogStructur                      As String
    Dim piutangLogPayLoad                       As String
    Dim piutangSaldoPayload                     As String
    
    Set rsPiutangSaldo = New ADODB.Recordset
    cunstructRsSaldoPayload
    
    piutangInfo.getSaldoByIdCust idCust, 25
    Set rsPiutangSaldo = piutangInfo.getSaldoInfoList
    payAmountBalance = val(Format(payAmountNett, "0.000"))
    
    
    If rsPiutangSaldo.RecordCount > 0 Then
        rsPiutangSaldo.Sort = "tanggal"
        rsPiutangSaldo.MoveFirst
        piutangLogStructur = cunstructPiutangLogTable
        
        With rsPiutangSaldo
            Do While .EOF = False
                If val(Format(payAmountBalance, "0.000")) > val(Format(!akhirNetto, "0.000")) Then
                    valAmount = val(Format(!akhirNetto, "0.000"))
                Else
                    valAmount = val(Format(payAmountBalance, "0.000"))
                End If
                
                If Len(piutangLogPayLoad) = 0 Then
                    piutangLogPayLoad = Trim(piutangLogPayLoad) & cunstructPiutangLogBathPayload(tglFaktur, credit, Trim(!id), val(Format(valAmount, "0.000")), val(Format(payAmountRp, "0.0")), Trim(idReff), ket)
                Else
                    piutangLogPayLoad = Trim(piutangLogPayLoad) & "," & cunstructPiutangLogBathPayload(tglFaktur, credit, Trim(!id), val(Format(valAmount, "0.000")), val(Format(payAmountRp, "0.0")), Trim(idReff), ket)
                End If
                
                addRecordToRsSaldoPayload cunstructPiutangSaldoPayload(credit, Trim(!id), val(Format(valAmount, "0.000")), val(Format(payAmountRp, "0.0")))
                payAmountBalance = val(Format(payAmountBalance, "0.000")) - val(Format(!akhirNetto, "0.000"))
                rsPiutangSaldo.MoveNext
            Loop
        End With
    End If
    
    piutangLogPayLoad = piutangLogStructur & piutangLogPayLoad
    payLoadPiutangLogBatch = piutangLogPayLoad
    
    Set rsPiutangSaldo = Nothing
    'Set payloadPiutangSaldoUpdtBatch = Nothing
End Function









Private Function getTransactionType(trxType As transactionType) As String
    Select Case trxType
           Case debit
                getTransactionType = "D"
           Case credit
                getTransactionType = "K"
    End Select
End Function

Private Function cunstructPiutangLogTable() As String
    cunstructPiutangLogTable = "INSERT INTO " & Trim(transactionLogTable) & "(tanggal,id_penjualan,transaction_type,netto,rp,no_reference,keterangan,created_by,created_date) VALUES "
End Function

Private Function cunstructPiutangLogBathPayload(tgl As Date, trxType As transactionType, idPenjualan As String, nett As Double, rp As Double, noref As String, ket As String) As String
    cunstructPiutangLogBathPayload = "(" & _
                                     "'" & Format(tgl, "YYYY-MM-DD") & "','" & Trim(idPenjualan) & "','" & Trim(getTransactionType(trxType)) & "','" & Format(nett, "0.000") & "','" & Format(rp, "0.0") & "' " & _
                                     ",'" & Trim(noref) & "','" & Trim(ket) & "','" & Trim(getCreatedBy) & "','" & Format(Now, "YYYY-MM-DD HH:MM:SS") & "'" & _
                                     ")"
End Function


Private Sub cunstructRsSaldoPayload()
    Set payloadPiutangSaldoUpdtBatch = New ADODB.Recordset
    payloadPiutangSaldoUpdtBatch.Fields.Append "value", adChar, 200
    payloadPiutangSaldoUpdtBatch.Open
End Sub

Private Function cunstructPiutangSaldoPayload(trxType As transactionType, idPenjualan As String, nett As Double, rp As Double) As String
    Select Case trxType
           Case credit
                cunstructPiutangSaldoPayload = "UPDATE ts_piutang SET " & _
                                   "out_netto=out_netto+'" & val(Format(nett, "0.000")) & "'" & _
                                   ",out_rp=out_rp+'" & val(Format(rp, "0.0")) & "'" & _
                                   ",akhir_netto=akhir_netto-'" & val(Format(nett, "0.000")) & "'" & _
                                   ",akhir_rp=akhir_rp-'" & val(Format(rp, "0.0")) & "'" & _
                                   " WHERE id_penjualan = '" & Trim(idPenjualan) & "';"
           Case debit
                cunstructPiutangSaldoPayload = "UPDATE ts_piutang SET " & _
                                   "in_netto=in_netto+'" & val(Format(nett, "0.000")) & "'" & _
                                   ",in_rp=in_rp+'" & val(Format(rp, "0.0")) & "'" & _
                                   ",akhir_netto=akhir_netto+'" & val(Format(nett, "0.000")) & "'" & _
                                   ",akhir_rp=akhir_rp+'" & val(Format(rp, "0.0")) & "'" & _
                                   " WHERE id_penjualan = '" & Trim(idPenjualan) & "' " & _
                                   ";"
    End Select
End Function

Private Sub addRecordToRsSaldoPayload(vaData As String)
    payloadPiutangSaldoUpdtBatch.AddNew
    payloadPiutangSaldoUpdtBatch.Fields("value") = Trim(vaData)
    payloadPiutangSaldoUpdtBatch.Update
End Sub


