VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsPiutangInfo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Private saldoInfoList               As ADODB.Recordset

Private conData                     As connection
Private recordsetLocal              As rsSaveToLocal


Private Sub Class_Initialize()
    Set conData = New connection
    conData.openConnection
    
    Set saldoInfoList = New ADODB.Recordset
    constructRsSaldo
End Sub

Public Function getSaldoInfoList() As ADODB.Recordset
    Set getSaldoInfoList = saldoInfoList
End Function
    

Public Function getSaldoByIdTrx(idTrx As String) As ADODB.Recordset
    Dim payLoad                 As String
    
    payLoad = "SELECT * FROM ts_piutang WHERE id_penjualan = '" & Trim(idTrx) & "'"
    Set getSaldoByIdTrx = conData.queryExecution(payLoad)
End Function

Public Function getSaldoByIdCust(idCust As String, limit As Integer)
    Dim payLoad                 As String
    Dim filterCust              As String
    Dim limitDataQuery          As String
    
    If Len(idCust) > 0 And Trim(idCust) <> "-" Then
        filterCust = " AND a.id_customer = '" & Trim(idCust) & "'"
    Else
        filterCust = " "
    End If
    
    If val(limit) <= 0 Then
        limitDataQuery = " LIMIT 100"
    Else
        limitDataQuery = " LIMIT " & limit
    End If
    
    Set recordsetLocal = New rsSaveToLocal
    payLoad = "SELECT a.tanggal,CONCAT(b.nama,'-',b.alamat) as cust_info,a.akhir_netto as akhirNetto,a.akhir_rp as akhirRp,a.id_penjualan as id FROM ts_piutang A JOIN md_customer B ON a.id_customer = b.id " & _
              "WHERE (a.akhir_netto > 0 or a.akhir_rp>0) " & filterCust & limitDataQuery
    Set saldoInfoList = conData.queryExecution(payLoad)
    recordsetLocal.saveRsToLocal saldoInfoList, "SaldoByIdCust"
    saldoInfoList.Close
    conData.closeConnection
    
    recordsetLocal.opeRsFromLocal ("SaldoByIdCust")
    Set saldoInfoList = recordsetLocal.getRsResult
    Set recordsetLocal = Nothing
End Function

Public Function getAllSaldoOngoing(limit As Integer)
    getSaldoByIdCust "-", limit
End Function







Private Sub constructRsSaldo()
    saldoInfoList.Fields.Append "id", adChar, 35
    saldoInfoList.Fields.Append "tanggal", adDate
    saldoInfoList.Fields.Append "custInfo", adChar, 35
    saldoInfoList.Fields.Append "akhirNetto", adDouble, 18
    saldoInfoList.Fields.Append "akhirRp", adDouble, 18
    saldoInfoList.Open
End Sub
