CREATE TABLE `db-seed`.`md_item` (
  `id` CHAR(15) NOT NULL,
  `nama_barang` VARCHAR(45) NOT NULL DEFAULT '-',
  `warna_barang` VARCHAR(45) NOT NULL DEFAULT '-',
  `kadar_barang` VARCHAR(45) NOT NULL DEFAULT '-',
  `tukar_jual` DOUBLE NOT NULL DEFAULT 0,
  `discount` DOUBLE NOT NULL DEFAULT 0,
  `jenis_barang` VARCHAR(45) NOT NULL DEFAULT '-',
  `status_barang` VARCHAR(10) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `kode_barang_UNIQUE` (`id` ASC) VISIBLE);